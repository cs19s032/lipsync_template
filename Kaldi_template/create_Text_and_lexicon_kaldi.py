#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import subprocess

txt_file = sys.argv[1]  # txt.done.data is the arguement. Make sure the text_ids are ordered(not anymore)

with open(txt_file) as FR:
    txt_lines = [lines.rstrip() for lines in FR]
txt_lines.sort()
txt_lines_parsed = []
for i in range(len(txt_lines)):
    txt_lines_parsed.extend([str.split(txt_lines[i])])

for i in range(len(txt_lines)):
    Text_train = open('data/train/text', 'a')
    Text_train.write(txt_lines_parsed[i][1] + ' sil ')

    # print txt_lines_parsed[i][1] + " sil ",

    for j in range(3, len(txt_lines_parsed[i]) - 2):
        if txt_lines_parsed[i][j] not in [',', '-', ';']:
            subprocess.getstatusoutput('valgrind ./unified-parser '
                    + txt_lines_parsed[i][j] + ' 1 0 0 0 ')
            subprocess.getstatusoutput('sh edit_pronunciation.sh'
                    )
            phone_transcript = open('wordpronunciation1', 'r')
            phone_trans_line = phone_transcript.readline()
            phone_transcript.close()
            lexicon = open('data/local/dict/lexicon.txt', 'a')
            subprocess.getstatusoutput('sh edit_pronunciation_2.sh'
                    )
            word = open('wordpronunciation2', 'r')
            word_line = word.readline()
            Text_train.write(word_line[:-1] + ' ')

        # print word_line[:-1],

            lexicon.write(word_line[:-1] + ' ' + phone_trans_line)
            lexicon.close()
            subprocess.getstatusoutput('rm wordpronunciation1 wordpronunciation2'
                    )
    Text_train.write('sil\n')

    # print "sil\n"

    Text_train.close()
print('text and lexicon created')

