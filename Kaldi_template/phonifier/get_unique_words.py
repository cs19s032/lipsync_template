from argparse import ArgumentParser
from tqdm import tqdm

start_char={}
start_char['ta']='ஂ'
start_char['te']='ఀ'
start_char['hi']='ऀ'
start_char['mr']='ऀ'
start_char['gu']='ઁ'
start_char['or']='ଁ'

end_char={}
end_char['ta']='ௗ'
end_char['te']='ౣ'
end_char['hi']='ॡ'
end_char['mr']='ॡ'
end_char['gu']='૿ૣ'
end_char['or']='ୣ'

def is_word_in_language(word, lang_code):
	
	for i in range(len(word)):
		#print(word[i])
		#print(start_char[args.lang_code])
		if word[i]<start_char[lang_code] or word[i]>end_char[lang_code]:
			return False
	return True




parser = ArgumentParser()
parser.add_argument('-o', '--output-file', type=str)
parser.add_argument('-f', '--file-list', nargs='+', default=[])
parser.add_argument('-l', '--lang-code', default=None, type=str)
parser.add_argument('-c', '--get-word-counts', default=False, type=bool)
parser.add_argument('-i', '--ignore-first-word', default=False, type=bool)




args = parser.parse_args()

print(args.output_file)
print(args.file_list)
print(args.ignore_first_word)

unique_words_dict = {}

for file_name in args.file_list:
	print("reading words from "+ file_name)
	file = open(file_name, 'r')
	for line in tqdm(file):
		line = line.strip()
		line = line.replace('\t', ' ')
		word_list = [x.strip() for x in line.split(' ')]
		if (args.ignore_first_word):
			word_list.pop(0)
		i=0
		junk_words=[]		
		for word in word_list:
			if word == '': 
				junk_words.append(i)
			elif "." in word:
				word_list = word_list + word.split(".")
				junk_words.append(i)
			elif word[-1] == ',' or word[-1] == '?':
				junk_words.append(i)
				word=word[:-1]
				word_list.append(word)
			i+=1
		#print([word_list[i] for i in junk_words])
		[word_list.pop(i) for i in  sorted(junk_words, reverse = True)]		
		for word in word_list:
			if args.lang_code==None or \
			is_word_in_language(word,args.lang_code):
				if word not in unique_words_dict:
					unique_words_dict[word] = 1
				elif args.get_word_counts:
					unique_words_dict[word]+=1
	file.close()

print("sorting words")	
unique_words=list(unique_words_dict.keys())
unique_words.sort()
print("writing words")

file = open(args.output_file, 'w')
for word in unique_words:
	if word != "":
		if not args.get_word_counts:
			file.write(word+'\n')
		else:
			file.write(word+' '+str(unique_words_dict[word])+'\n')
file.close
