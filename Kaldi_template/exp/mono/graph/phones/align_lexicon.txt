!SIL !SIL sil_S
<eps> <eps> sil
aadhaar aadhaar aa_B dh_I aa_I r_E
aaeegaa aaeegaa aa_B ee_I g_I aa_E
aaeeqgee aaeeqgee aa_B ee_I q_I g_I ee_E
aagee aagee aa_B g_I ee_E
aaiee aaiee aa_B i_I ee_E
aaii aaii aa_B ii_E
aaitxam aaitxam aa_B i_I tx_I a_I m_E
aaitxams aaitxams aa_B i_I tx_I a_I m_I s_E
aaj aaj aa_B j_E
aam aam aa_B m_E
aamtour aamtour aa_B m_I t_I ou_I r_E
aao aao aa_B o_E
aap aap aa_B p_E
aapkaa aapkaa aa_B p_I k_I aa_E
aapkee aapkee aa_B p_I k_I ee_E
aapko aapko aa_B p_I k_I o_E
aardxar aardxar aa_B r_I dx_I a_I r_E
aargumeeqtx aargumeeqtx aa_B r_I g_I u_I m_I ee_I q_I tx_E
aargyuumeeqtxs aargyuumeeqtxs aa_B r_I g_I y_I uu_I m_I ee_I q_I tx_I s_E
aasaan aasaan aa_B s_I aa_I n_E
aasapaas aasapaas aa_B s_I a_I p_I aa_I s_E
aataa aataa aa_B t_I aa_E
aatee aatee aa_B t_I ee_E
aatii aatii aa_B t_I ii_E
aatxh aatxh aa_B txh_E
aautx aautx aa_B u_I tx_E
aautxputx aautxputx aa_B u_I tx_I p_I u_I tx_E
aawashyak aawashyak aa_B w_I a_I sh_I y_I a_I k_E
aawashyaktaa aawashyaktaa aa_B w_I a_I sh_I y_I a_I k_I t_I aa_E
ab ab a_B b_E
abhii abhii a_B bh_I ii_E
acchaa acchaa a_B c_I ch_I aa_E
acchii acchii a_B c_I ch_I ii_E
adhik adhik a_B dh_I i_I k_E
adhikaaqsh adhikaaqsh a_B dh_I i_I k_I aa_I q_I sh_E
agar agar a_B g_I a_I r_E
aglaa aglaa a_B g_I l_I aa_E
aglii aglii a_B g_I l_I ii_E
ajiib ajiib a_B j_I ii_I b_E
alaawaa alaawaa a_B l_I aa_I w_I aa_E
alag alag a_B l_I a_I g_E
algoritham algoritham a_B l_I g_I o_I r_I i_I th_I a_I m_E
algorithmik algorithmik a_B l_I g_I o_I r_I i_I th_I m_I i_I k_E
algorithms algorithms a_B l_I g_I o_I r_I i_I th_I m_I s_E
aniwaarya aniwaarya a_B n_I i_I w_I aa_I r_I y_I a_E
anoupcaarik anoupcaarik a_B n_I ou_I p_I c_I aa_I r_I i_I k_E
anubhwa anubhwa a_B n_I u_I bh_I w_I a_E
anya anya a_B n_I y_I a_E
apar apar a_B p_I a_I r_E
apdxeetx apdxeetx a_B p_I dx_I ee_I tx_E
apeeksxit apeeksxit a_B p_I ee_I k_I sx_I i_I t_E
apeenxdx apeenxdx a_B p_I ee_I nx_I dx_E
apnaa apnaa a_B p_I n_I aa_E
apnee apnee a_B p_I n_I ee_E
aqt aqt a_B q_I t_E
aqtar aqtar a_B q_I t_I a_I r_E
aqtim aqtim a_B q_I t_I i_I m_E
arithmeetxiklii arithmeetxiklii a_B r_I i_I th_I m_I ee_I tx_I i_I k_I l_I ii_E
arth arth a_B r_I th_E
asaain asaain a_B s_I aa_I i_I n_E
aseeqdxiqg aseeqdxiqg a_B s_I ee_I q_I dx_I i_I q_I g_E
asiim asiim a_B s_I ii_I m_E
atxhaarah atxhaarah a_B txh_I aa_I r_I a_I h_E
awalokan awalokan a_B w_I a_I l_I o_I k_I a_I n_E
axf axf ax_B f_E
axpreeshan axpreeshan ax_B p_I r_I ee_I sh_I a_I n_E
axptximaaijeeshan axptximaaijeeshan ax_B p_I tx_I i_I m_I aa_I i_I j_I ee_I sh_I a_I n_E
axptximam axptximam ax_B p_I tx_I i_I m_I a_I m_E
axrdxars axrdxars ax_B r_I dx_I a_I r_I s_E
baad baad b_B aa_I d_E
baaii baaii b_B aa_I ii_E
baaiiq baaiiq b_B aa_I ii_I q_E
baar baar b_B aa_I r_E
baarah baarah b_B aa_I r_I a_I h_E
baaree baaree b_B aa_I r_I ee_E
baarii baarii b_B aa_I r_I ii_E
baat baat b_B aa_I t_E
baatoq baatoq b_B aa_I t_I o_I q_E
bacaa bacaa b_B a_I c_I aa_E
badalnaa badalnaa b_B a_I d_I a_I l_I n_I aa_E
baddh baddh b_B a_I d_I dh_E
badxhqaa badxhqaa b_B a_I dxhq_I aa_E
badxqaa badxqaa b_B a_I dxq_I aa_E
badxqee badxqee b_B a_I dxq_I ee_E
badxqii badxqii b_B a_I dxq_I ii_E
bahut bahut b_B a_I h_I u_I t_E
bajaaya bajaaya b_B a_I j_I aa_I y_I a_E
balki balki b_B a_I l_I k_I i_E
banaatee banaatee b_B a_I n_I aa_I t_I ee_E
banaatii banaatii b_B a_I n_I aa_I t_I ii_E
baraabar baraabar b_B a_I r_I aa_I b_I a_I r_E
bashartee bashartee b_B a_I sh_I a_I r_I t_I ee_E
bataataa bataataa b_B a_I t_I aa_I t_I aa_E
beehtar beehtar b_B ee_I h_I t_I a_I r_E
beeshak beeshak b_B ee_I sh_I a_I k_E
beesik beesik b_B ee_I s_I i_I k_E
bhaasxaaoq bhaasxaaoq bh_B aa_I sx_I aa_I o_I q_E
bhalee bhalee bh_B a_I l_I ee_E
bhii bhii bh_B ii_E
bholaa bholaa bh_B o_I l_I aa_E
bii bii b_B ii_E
biic biic b_B ii_I c_E
bilkul bilkul b_B i_I l_I k_I u_I l_E
biltxain biltxain b_B i_I l_I tx_I a_I i_I n_E
binaa binaa b_B i_I n_I aa_E
biqdu biqdu b_B i_I q_I d_I u_E
bitx bitx b_B i_I tx_E
breikeetx breikeetx b_B r_I ei_I k_I ee_I tx_E
breikeetxs breikeetxs b_B r_I ei_I k_I ee_I tx_I s_E
buk buk b_B u_I k_E
bukiqg bukiqg b_B u_I k_I i_I q_I g_E
caah caah c_B aa_I h_E
caahiee caahiee c_B aa_I h_I i_I ee_E
caahtaa caahtaa c_B aa_I h_I t_I aa_E
caahtee caahtee c_B aa_I h_I t_I ee_E
caar caar c_B aa_I r_E
calaanee calaanee c_B a_I l_I aa_I n_I ee_E
caliee caliee c_B a_I l_I i_I ee_E
calnee calnee c_B a_I l_I n_I ee_E
caltee caltee c_B a_I l_I t_I ee_E
catur catur c_B a_I t_I u_I r_E
ceis ceis c_B ei_I s_E
chah chah ch_B a_I h_E
chodxq chodxq ch_B o_I dxq_E
chotxaa chotxaa ch_B o_I tx_I aa_E
chotxee chotxee ch_B o_I tx_I ee_E
ciijeeq ciijeeq c_B ii_I j_I ee_I q_E
ciijoq ciijoq c_B ii_I j_I o_I q_E
citrit citrit c_B i_I t_I r_I i_I t_E
coudah coudah c_B ou_I d_I a_I h_E
cuneeq cuneeq c_B u_I n_I ee_I q_E
cuuqki cuuqki c_B uu_I q_I k_I i_E
daaiiq daaiiq d_B aa_I ii_I q_E
daayree daayree d_B aa_I y_I r_I ee_E
darshaanee darshaanee d_B a_I r_I sh_I aa_I n_I ee_E
darshaatee darshaatee d_B a_I r_I sh_I aa_I t_I ee_E
das das d_B a_I s_E
deekh deekh d_B ee_I kh_E
deekhaa deekhaa d_B ee_I kh_I aa_E
deekhee deekhee d_B ee_I kh_I ee_E
deekheeq deekheeq d_B ee_I kh_I ee_I q_E
deekheeqgee deekheeqgee d_B ee_I kh_I ee_I q_I g_I ee_E
deekhnaa deekhnaa d_B ee_I kh_I n_I aa_E
deekhtaa deekhtaa d_B ee_I kh_I t_I aa_E
deekhtee deekhtee d_B ee_I kh_I t_I ee_E
deenaa deenaa d_B ee_I n_I aa_E
deenee deenee d_B ee_I n_I ee_E
deeq deeq d_B ee_I q_E
deeqgee deeqgee d_B ee_I q_I g_I ee_E
deetaa deetaa d_B ee_I t_I aa_E
deetee deetee d_B ee_I t_I ee_E
deetii deetii d_B ee_I t_I ii_E
dhaarnxaa dhaarnxaa dh_B aa_I r_I nx_I aa_E
dhyaan dhyaan dh_B y_I aa_I n_E
diee diee d_B i_I ee_E
dikhaaii dikhaaii d_B i_I kh_I aa_I ii_E
din din d_B i_I n_E
diwaaidxs diwaaidxs d_B i_I w_I aa_I i_I dx_I s_E
do do d_B o_E
dohraaee dohraaee d_B o_I h_I r_I aa_I ee_E
donoq donoq d_B o_I n_I o_I q_E
drqshyoq drqshyoq d_B rq_I sh_I y_I o_I q_E
duusraa duusraa d_B uu_I s_I r_I aa_E
duusree duusree d_B uu_I s_I r_I ee_E
duusrii duusrii d_B uu_I s_I r_I ii_E
dwaaraa dwaaraa d_B w_I aa_I r_I aa_E
dxaal dxaal dx_B aa_I l_E
dxaaleeq dxaaleeq dx_B aa_I l_I ee_I q_E
dxaatxaa dxaatxaa dx_B aa_I tx_I aa_E
dxaxkyuumeeqtx dxaxkyuumeeqtx dx_B ax_I k_I y_I uu_I m_I ee_I q_I tx_E
dxeeph dxeeph dx_B ee_I ph_E
dxeephinishan dxeephinishan dx_B ee_I ph_I i_I n_I i_I sh_I a_I n_E
dxeeskeendxiqg dxeeskeendxiqg dx_B ee_I s_I k_I ee_I n_I dx_I i_I q_I g_E
dxeetxaa dxeetxaa dx_B ee_I tx_I aa_E
dxhaqg dxhaqg dxh_B a_I q_I g_E
dxhuuqdxhatee dxhuuqdxhatee dxh_B uu_I q_I dxh_I a_I t_I ee_E
dxifaain dxifaain dx_B i_I f_I aa_I i_I n_E
dxii dxii dx_B ii_E
dxiphaain dxiphaain dx_B i_I ph_I aa_I i_I n_E
dxish dxish dx_B i_I sh_E
dxiwaaidx dxiwaaidx dx_B i_I w_I aa_I i_I dx_E
dxiwaaidxeedx dxiwaaidxeedx dx_B i_I w_I aa_I i_I dx_I ee_I dx_E
dxiwaaizar dxiwaaizar dx_B i_I w_I aa_I i_I z_I a_I r_E
dxiwaaizars dxiwaaizars dx_B i_I w_I aa_I i_I z_I a_I r_I s_E
dxiwiizan dxiwiizan dx_B i_I w_I ii_I z_I a_I n_E
ee ee ee_S
eek eek ee_B k_E
eeks eeks ee_B k_I s_E
eekseekyuutx eekseekyuutx ee_B k_I s_I ee_I k_I y_I uu_I tx_E
eelgoridam eelgoridam ee_B l_I g_I o_I r_I i_I d_I a_I m_E
eelgoridmik eelgoridmik ee_B l_I g_I o_I r_I i_I d_I m_I i_I k_E
eelimeeqtx eelimeeqtx ee_B l_I i_I m_I ee_I q_I tx_E
eem eem ee_B m_E
eemptxii eemptxii ee_B m_I p_I tx_I ii_E
een een ee_B n_E
eeph eeph ee_B ph_E
eeqdx eeqdx ee_B q_I dx_E
eesaeemaees eesaeemaees ee_B s_I a_I ee_I m_I a_I ee_I s_E
eewrii eewrii ee_B w_I r_I ii_E
eidx eidx ei_B dx_E
eisaa eisaa ei_B s_I aa_E
eisee eisee ei_B s_I ee_E
eisii eisii ei_B s_I ii_E
gaaraqtxii gaaraqtxii g_B aa_I r_I a_I q_I tx_I ii_E
gaee gaee g_B a_I ee_E
gaii gaii g_B a_I ii_E
ganxit ganxit g_B a_I nx_I i_I t_E
gatxhan gatxhan g_B a_I txh_I a_I n_E
gayaa gayaa g_B a_I y_I aa_E
geems geems g_B ee_I m_I s_E
gintee gintee g_B i_I n_I t_I ee_E
greetxeestx greetxeestx g_B r_I ee_I tx_I ee_I s_I tx_E
gucchaa gucchaa g_B u_I c_I ch_I aa_E
gujartee gujartee g_B u_I j_I a_I r_I t_I ee_E
gujree gujree g_B u_I j_I r_I ee_E
gunaa gunaa g_B u_I n_I aa_E
gunxaa gunxaa g_B u_I nx_I aa_E
guzree guzree g_B u_I z_I r_I ee_E
haaii haaii h_B aa_I ii_E
haalaaqki haalaaqki h_B aa_I l_I aa_I q_I k_I i_E
haaq haaq h_B aa_I q_E
haasil haasil h_B aa_I s_I i_I l_E
haath haath h_B aa_I th_E
hal hal h_B a_I l_E
ham ham h_B a_I m_E
hamaaraa hamaaraa h_B a_I m_I aa_I r_I aa_E
hamaaree hamaaree h_B a_I m_I aa_I r_I ee_E
hamaarii hamaarii h_B a_I m_I aa_I r_I ii_E
hameeq hameeq h_B a_I m_I ee_I q_E
hameeshaa hameeshaa h_B a_I m_I ee_I sh_I aa_E
hamnee hamnee h_B a_I m_I n_I ee_E
har har h_B a_I r_E
haxl haxl h_B ax_I l_E
hei hei h_B ei_E
heiq heiq h_B ei_I q_E
hii hii h_B ii_E
hissaa hissaa h_B i_I s_I s_I aa_E
ho ho h_B o_E
hogaa hogaa h_B o_I g_I aa_E
hogii hogii h_B o_I g_I ii_E
honaa honaa h_B o_I n_I aa_E
honee honee h_B o_I n_I ee_E
hoq hoq h_B o_I q_E
hoqgee hoqgee h_B o_I q_I g_I ee_E
hotaa hotaa h_B o_I t_I aa_E
hotee hotee h_B o_I t_I ee_E
hotii hotii h_B o_I t_I ii_E
huee huee h_B u_I ee_E
huuq huuq h_B uu_I q_E
ikkiis ikkiis i_B k_I k_I ii_I s_E
ikwaalitxii ikwaalitxii i_B k_I w_I aa_I l_I i_I tx_I ii_E
in in i_B n_E
inheeq inheeq i_B n_I h_I ee_I q_E
inmee inmee i_B n_I m_I ee_E
inmeeq inmeeq i_B n_I m_I ee_I q_E
inphaarmeeshan inphaarmeeshan i_B n_I ph_I aa_I r_I m_I ee_I sh_I a_I n_E
insee insee i_B n_I s_I ee_E
iqdxeeqtxeedx iqdxeeqtxeedx i_B q_I dx_I ee_I q_I tx_I ee_I dx_E
iqglish iqglish i_B q_I g_I l_I i_I sh_E
iqgreedxieeqtxs iqgreedxieeqtxs i_B q_I g_I r_I ee_I dx_I i_I ee_I q_I tx_I s_E
iqstxrakshaqs iqstxrakshaqs i_B q_I s_I tx_I r_I a_I k_I sh_I a_I q_I s_E
iqteejras iqteejras i_B q_I t_I ee_I j_I r_I a_I s_E
iqtxramiidxieetx iqtxramiidxieetx i_B q_I tx_I r_I a_I m_I ii_I dx_I i_I ee_I tx_E
is is i_B s_E
isakaa isakaa i_B s_I a_I k_I aa_E
isakee isakee i_B s_I a_I k_I ee_E
isaliee isaliee i_B s_I a_I l_I i_I ee_E
isameeq isameeq i_B s_I a_I m_I ee_I q_E
isee isee i_B s_I ee_E
isii isii i_B s_I ii_E
isteemaal isteemaal i_B s_I t_I ee_I m_I aa_I l_E
itnaa itnaa i_B t_I n_I aa_E
itnee itnee i_B t_I n_I ee_E
jaa jaa j_B aa_E
jaaee jaaee j_B aa_I ee_E
jaaeegaa jaaeegaa j_B aa_I ee_I g_I aa_E
jaaeegii jaaeegii j_B aa_I ee_I g_I ii_E
jaahir jaahir j_B aa_I h_I i_I r_E
jaakee jaakee j_B aa_I k_I ee_E
jaamqc jaamqc j_B aa_I mq_I c_E
jaanaa jaanaa j_B aa_I n_I aa_E
jaankaarii jaankaarii j_B aa_I n_I k_I aa_I r_I ii_E
jaannaa jaannaa j_B aa_I n_I n_I aa_E
jaannee jaannee j_B aa_I n_I n_I ee_E
jaantee jaantee j_B aa_I n_I t_I ee_E
jaaqc jaaqc j_B aa_I q_I c_E
jaaqcataa jaaqcataa j_B aa_I q_I c_I a_I t_I aa_E
jaargan jaargan j_B aa_I r_I g_I a_I n_E
jaataa jaataa j_B aa_I t_I aa_E
jaatee jaatee j_B aa_I t_I ee_E
jab jab j_B a_I b_E
jagah jagah j_B a_I g_I a_I h_E
jaghoq jaghoq j_B a_I g_I h_I o_I q_E
jahaaq jahaaq j_B a_I h_I aa_I q_E
jaruurat jaruurat j_B a_I r_I uu_I r_I a_I t_E
jatxil jatxil j_B a_I tx_I i_I l_E
jawaab jawaab j_B a_I w_I aa_I b_E
jee jee j_B ee_E
jeedx jeedx j_B ee_I dx_E
jeisaa jeisaa j_B ei_I s_I aa_E
jeisee jeisee j_B ei_I s_I ee_E
jiisiidxii jiisiidxii j_B ii_I s_I ii_I dx_I ii_E
jiiwan jiiwan j_B ii_I w_I a_I n_E
jinheeq jinheeq j_B i_I n_I h_I ee_I q_E
jinhoqnee jinhoqnee j_B i_I n_I h_I o_I q_I n_I ee_E
jinkii jinkii j_B i_I n_I k_I ii_E
jis jis j_B i_I s_E
jisee jisee j_B i_I s_I ee_E
jiskaa jiskaa j_B i_I s_I k_I aa_E
jo jo j_B o_E
jodxq jodxq j_B o_I dxq_E
jodxqaa jodxqaa j_B o_I dxq_I aa_E
jodxqeeq jodxqeeq j_B o_I dxq_I ee_I q_E
jodxqii jodxqii j_B o_I dxq_I ii_E
jodxqtaa jodxqtaa j_B o_I dxq_I t_I aa_E
jodxqtee jodxqtee j_B o_I dxq_I t_I ee_E
kaa kaa k_B aa_E
kaam kaam k_B aa_I m_E
kaarak kaarak k_B aa_I r_I a_I k_E
kaaranx kaaranx k_B aa_I r_I a_I nx_E
kaarya kaarya k_B aa_I r_I y_I a_E
kaauqtx kaauqtx k_B aa_I u_I q_I tx_E
kabhii kabhii k_B a_I bh_I ii_E
kadmoq kadmoq k_B a_I d_I m_I o_I q_E
kah kah k_B a_I h_E
kahaa kahaa k_B a_I h_I aa_E
kaheegaa kaheegaa k_B a_I h_I ee_I g_I aa_E
kaheeqgee kaheeqgee k_B a_I h_I ee_I q_I g_I ee_E
kahiiq kahiiq k_B a_I h_I ii_I q_E
kahnaa kahnaa k_B a_I h_I n_I aa_E
kahtaa kahtaa k_B a_I h_I t_I aa_E
kahtee kahtee k_B a_I h_I t_I ee_E
kaii kaii k_B a_I ii_E
kaleekshan kaleekshan k_B a_I l_I ee_I k_I sh_I a_I n_E
kaleekshaqs kaleekshaqs k_B a_I l_I ee_I k_I sh_I a_I q_I s_E
kalpanaa kalpanaa k_B a_I l_I p_I a_I n_I aa_E
kam kam k_B a_I m_E
kamobeesh kamobeesh k_B a_I m_I o_I b_I ee_I sh_E
kampyuutx kampyuutx k_B a_I m_I p_I y_I uu_I tx_E
kampyuutxars kampyuutxars k_B a_I m_I p_I y_I uu_I tx_I a_I r_I s_E
kampyuutxeeshan kampyuutxeeshan k_B a_I m_I p_I y_I uu_I tx_I ee_I sh_I a_I n_E
kampyuutxeeshnal kampyuutxeeshnal k_B a_I m_I p_I y_I uu_I tx_I ee_I sh_I n_I a_I l_E
kamree kamree k_B a_I m_I r_I ee_E
kaqdxiishan kaqdxiishan k_B a_I q_I dx_I ii_I sh_I a_I n_E
kaqdxiishnal kaqdxiishnal k_B a_I q_I dx_I ii_I sh_I n_I a_I l_E
kaqpyuutx kaqpyuutx k_B a_I q_I p_I y_I uu_I tx_E
kaqpyuutxar kaqpyuutxar k_B a_I q_I p_I y_I uu_I tx_I a_I r_E
kaqpyuutxeeshans kaqpyuutxeeshans k_B a_I q_I p_I y_I uu_I tx_I ee_I sh_I a_I n_I s_E
kaqpyuutxiqg kaqpyuutxiqg k_B a_I q_I p_I y_I uu_I tx_I i_I q_I g_E
kar kar k_B a_I r_E
karee karee k_B a_I r_I ee_E
kareegaa kareegaa k_B a_I r_I ee_I g_I aa_E
kareeq kareeq k_B a_I r_I ee_I q_E
kareeqgee kareeqgee k_B a_I r_I ee_I q_I g_I ee_E
karkee karkee k_B a_I r_I k_I ee_E
karnaa karnaa k_B a_I r_I n_I aa_E
karnee karnee k_B a_I r_I n_I ee_E
karnii karnii k_B a_I r_I n_I ii_E
kartaa kartaa k_B a_I r_I t_I aa_E
kartee kartee k_B a_I r_I t_I ee_E
kartii kartii k_B a_I r_I t_I ii_E
kaxl kaxl k_B ax_I l_E
kaxlam kaxlam k_B ax_I l_I a_I m_E
kaxmaa kaxmaa k_B ax_I m_I aa_E
kaxman kaxman k_B ax_I m_I a_I n_E
kaxstx kaxstx k_B ax_I s_I tx_E
kee kee k_B ee_E
keewal keewal k_B ee_I w_I a_I l_E
keisee keisee k_B ei_I s_I ee_E
khaalii khaalii kh_B aa_I l_I ii_E
khaanaa khaanaa kh_B aa_I n_I aa_E
khaataa khaataa kh_B aa_I t_I aa_E
kheel kheel kh_B ee_I l_E
khilaaph khilaaph kh_B i_I l_I aa_I ph_E
kho kho kh_B o_E
khojeeq khojeeq kh_B o_I j_I ee_I q_E
khojnaa khojnaa kh_B o_I j_I n_I aa_E
khojnee khojnee kh_B o_I j_I n_I ee_E
khqaasiyat khqaasiyat khq_B aa_I s_I i_I y_I a_I t_E
khud khud kh_B u_I d_E
ki ki k_B i_E
kiee kiee k_B i_I ee_E
kii kii k_B ii_E
kisii kisii k_B i_I s_I ii_E
kitnaa kitnaa k_B i_I t_I n_I aa_E
kiyaa kiyaa k_B i_I y_I aa_E
kiyee kiyee k_B i_I y_I ee_E
klojdx klojdx k_B l_I o_I j_I dx_E
ko ko k_B o_E
kodx kodx k_B o_I dx_E
koii koii k_B o_I ii_E
kolan kolan k_B o_I l_I a_I n_E
kolons kolons k_B o_I l_I o_I n_I s_E
koshish koshish k_B o_I sh_I i_I sh_E
koun koun k_B ou_I n_E
kram kram k_B r_I a_I m_E
ksxamtaa ksxamtaa k_B sx_I a_I m_I t_I aa_E
kuch kuch k_B u_I ch_E
kursiyaamq kursiyaamq k_B u_I r_I s_I i_I y_I aa_I mq_E
kursiyoq kursiyoq k_B u_I r_I s_I i_I y_I o_I q_E
kushal kushal k_B u_I sh_I a_I l_E
kwaaqtxitxii kwaaqtxitxii k_B w_I aa_I q_I tx_I i_I tx_I ii_E
kyaa kyaa k_B y_I aa_E
kyoq kyoq k_B y_I o_I q_E
kyoqki kyoqki k_B y_I o_I q_I k_I i_E
laain laain l_B aa_I i_I n_E
lag lag l_B a_I g_E
lagaa lagaa l_B a_I g_I aa_E
lagbhag lagbhag l_B a_I g_I bh_I a_I g_E
laksxya laksxya l_B a_I k_I sx_I y_I a_E
laqbaa laqbaa l_B a_I q_I b_I aa_E
laxng laxng l_B ax_I n_I g_E
lee lee l_B ee_E
leekin leekin l_B ee_I k_I i_I n_E
leenaa leenaa l_B ee_I n_I aa_E
leenee leenee l_B ee_I n_I ee_E
leeq leeq l_B ee_I q_E
leetaa leetaa l_B ee_I t_I aa_E
leiqgweej leiqgweej l_B ei_I q_I g_I w_I ee_I j_E
liee liee l_B i_I ee_E
likh likh l_B i_I kh_E
likhaa likhaa l_B i_I kh_I aa_E
likheeq likheeq l_B i_I kh_I ee_I q_E
likhii likhii l_B i_I kh_I ii_E
likhnaa likhnaa l_B i_I kh_I n_I aa_E
likhnee likhnee l_B i_I kh_I n_I ee_E
likhtee likhtee l_B i_I kh_I t_I ee_E
limitx limitx l_B i_I m_I i_I tx_E
listx listx l_B i_I s_I tx_E
listxs listxs l_B i_I s_I tx_I s_E
log log l_B o_I g_E
logoq logoq l_B o_I g_I o_I q_E
loutxaaeeq loutxaaeeq l_B ou_I tx_I aa_I ee_I q_E
loutxaanaa loutxaanaa l_B ou_I tx_I aa_I n_I aa_E
maainas maainas m_B aa_I i_I n_I a_I s_E
maamlee maamlee m_B aa_I m_I l_I ee_E
maarks maarks m_B aa_I r_I k_I s_E
mahatwapuurnx mahatwapuurnx m_B a_I h_I a_I t_I w_I a_I p_I uu_I r_I nx_E
mahsuus mahsuus m_B a_I h_I s_I uu_I s_E
manipyuleetx manipyuleetx m_B a_I n_I i_I p_I y_I u_I l_I ee_I tx_E
manmaanee manmaanee m_B a_I n_I m_I aa_I n_I ee_E
maqc maqc m_B a_I q_I c_E
mashiin mashiin m_B a_I sh_I ii_I n_E
matlab matlab m_B a_I t_I l_I a_I b_E
meel meel m_B ee_I l_E
meeq meeq m_B ee_I q_E
meeqtx meeqtx m_B ee_I q_I tx_E
meinipuleetx meinipuleetx m_B ei_I n_I i_I p_I u_I l_I ee_I tx_E
meiq meiq m_B ei_I q_E
mileegaa mileegaa m_B i_I l_I ee_I g_I aa_E
milnaa milnaa m_B i_I l_I n_I aa_E
miltaa miltaa m_B i_I l_I t_I aa_E
minimam minimam m_B i_I n_I i_I m_I a_I m_E
moujuudaa moujuudaa m_B ou_I j_I uu_I d_I aa_E
mujhee mujhee m_B u_I jh_I ee_E
mukhya mukhya m_B u_I kh_I y_I a_E
mushkil mushkil m_B u_I sh_I k_I i_I l_E
muul muul m_B uu_I l_E
na na n_B a_E
naa naa n_B aa_E
naain naain n_B aa_I i_I n_E
naamak naamak n_B aa_I m_I a_I k_E
naee naee n_B a_I ee_E
nahiiq nahiiq n_B a_I h_I ii_I q_E
nambar nambar n_B a_I m_I b_I a_I r_E
naqbar naqbar n_B a_I q_I b_I a_I r_E
naqbaroq naqbaroq n_B a_I q_I b_I a_I r_I o_I q_E
naqbars naqbars n_B a_I q_I b_I a_I r_I s_E
nayaa nayaa n_B a_I y_I aa_E
nazar nazar n_B a_I z_I a_I r_E
nee nee n_B ee_E
neem neem n_B ee_I m_E
neems neems n_B ee_I m_I s_E
nikaal nikaal n_B i_I k_I aa_I l_E
nikaalaa nikaalaa n_B i_I k_I aa_I l_I aa_E
nirbhar nirbhar n_B i_I r_I bh_I a_I r_E
nirdeesh nirdeesh n_B i_I r_I d_I ee_I sh_E
nirmaanx nirmaanx n_B i_I r_I m_I aa_I nx_E
nishcit nishcit n_B i_I sh_I c_I i_I t_E
nisxkarsx nisxkarsx n_B i_I sx_I k_I a_I r_I sx_E
nou nou n_B ou_E
nyuumeerikal nyuumeerikal n_B y_I uu_I m_I ee_I r_I i_I k_I a_I l_E
opan opan o_B p_I a_I n_E
or or o_B r_E
oupcaarik oupcaarik ou_B p_I c_I aa_I r_I i_I k_E
our our ou_B r_E
paa paa p_B aa_E
paaeeqgee paaeeqgee p_B aa_I ee_I q_I g_I ee_E
paaithan paaithan p_B aa_I i_I th_I a_I n_E
paamqc paamqc p_B aa_I mq_I c_E
paaqc paaqc p_B aa_I q_I c_E
paar paar p_B aa_I r_E
paas paas p_B aa_I s_E
paatee paatee p_B aa_I t_I ee_E
paatxhyakram paatxhyakram p_B aa_I txh_I y_I a_I k_I r_I a_I m_E
paawar paawar p_B aa_I w_I a_I r_E
paaythan paaythan p_B aa_I y_I th_I a_I n_E
pacciis pacciis p_B a_I c_I c_I ii_I s_E
padxhq padxhq p_B a_I dxhq_E
padxhqaa padxhqaa p_B a_I dxhq_I aa_E
padxhqnee padxhqnee p_B a_I dxhq_I n_I ee_E
padxqtaa padxqtaa p_B a_I dxq_I t_I aa_E
pahcaan pahcaan p_B a_I h_I c_I aa_I n_E
pahlaa pahlaa p_B a_I h_I l_I aa_E
pahlee pahlee p_B a_I h_I l_I ee_E
pahlii pahlii p_B a_I h_I l_I ii_E
pakaanee pakaanee p_B a_I k_I aa_I n_I ee_E
paqkcueeshan paqkcueeshan p_B a_I q_I k_I c_I u_I ee_I sh_I a_I n_E
paqkti paqkti p_B a_I q_I k_I t_I i_E
paqktiyoq paqktiyoq p_B a_I q_I k_I t_I i_I y_I o_I q_E
par par p_B a_I r_E
parantu parantu p_B a_I r_I a_I n_I t_I u_E
paribhaasxaa paribhaasxaa p_B a_I r_I i_I bh_I aa_I sx_I aa_E
paribhaasxit paribhaasxit p_B a_I r_I i_I bh_I aa_I sx_I i_I t_E
paricit paricit p_B a_I r_I i_I c_I i_I t_E
pariiksxanx pariiksxanx p_B a_I r_I ii_I k_I sx_I a_I nx_E
parseeqtx parseeqtx p_B a_I r_I s_I ee_I q_I tx_E
parseeqtxeej parseeqtxeej p_B a_I r_I s_I ee_I q_I tx_I ee_I j_E
parwaah parwaah p_B a_I r_I w_I aa_I h_E
paryaapt paryaapt p_B a_I r_I y_I aa_I p_I t_E
pataa pataa p_B a_I t_I aa_E
paxiqtx paxiqtx p_B ax_I i_I q_I tx_E
paxiqtxs paxiqtxs p_B ax_I i_I q_I tx_I s_E
paxjitxiwa paxjitxiwa p_B ax_I j_I i_I tx_I i_I w_I a_E
peeshkash peeshkash p_B ee_I sh_I k_I a_I sh_E
phaainaaitx phaainaaitx ph_B aa_I i_I n_I aa_I i_I tx_E
phaqkshan phaqkshan ph_B a_I q_I k_I sh_I a_I n_E
phaqkshans phaqkshans ph_B a_I q_I k_I sh_I a_I n_I s_E
pheeqk pheeqk ph_B ee_I q_I k_E
pheiktxar pheiktxar ph_B ei_I k_I tx_I a_I r_E
pheiktxars pheiktxars ph_B ei_I k_I tx_I a_I r_I s_E
pheiktxarsoq pheiktxarsoq ph_B ei_I k_I tx_I a_I r_I s_I o_I q_E
phir phir ph_B i_I r_E
phon phon ph_B o_I n_E
pichlee pichlee p_B i_I ch_I l_I ee_E
pichlii pichlii p_B i_I ch_I l_I ii_E
plas plas p_B l_I a_I s_E
posishan posishan p_B o_I s_I i_I sh_I a_I n_E
posishans posishans p_B o_I s_I i_I sh_I a_I n_I s_E
praapt praapt p_B r_I aa_I p_I t_E
prakaar prakaar p_B r_I a_I k_I aa_I r_E
prakriyaa prakriyaa p_B r_I a_I k_I r_I i_I y_I aa_E
prastut prastut p_B r_I a_I s_I t_I u_I t_E
pratidin pratidin p_B r_I a_I t_I i_I d_I i_I n_E
pratiit pratiit p_B r_I a_I t_I ii_I t_E
pratipaadan pratipaadan p_B r_I a_I t_I i_I p_I aa_I d_I a_I n_E
pratyeek pratyeek p_B r_I a_I t_I y_I ee_I k_E
prayaas prayaas p_B r_I a_I y_I aa_I s_E
probleems probleems p_B r_I o_I b_I l_I ee_I m_I s_E
prograam prograam p_B r_I o_I g_I r_I aa_I m_E
prograamiqg prograamiqg p_B r_I o_I g_I r_I aa_I m_I i_I q_I g_E
prograams prograams p_B r_I o_I g_I r_I aa_I m_I s_E
proseesar proseesar p_B r_I o_I s_I ee_I s_I a_I r_E
punargatxhit punargatxhit p_B u_I n_I a_I r_I g_I a_I txh_I i_I t_E
punarwyawasthit punarwyawasthit p_B u_I n_I a_I r_I w_I y_I a_I w_I a_I s_I th_I i_I t_E
punaupyog punaupyog p_B u_I n_I a_I u_I p_I y_I o_I g_E
puraanaa puraanaa p_B u_I r_I aa_I n_I aa_E
puraanee puraanee p_B u_I r_I aa_I n_I ee_E
puuchtee puuchtee p_B uu_I ch_I t_I ee_E
puuraa puuraa p_B uu_I r_I aa_E
puurii puurii p_B uu_I r_I ii_E
raaitxmostx raaitxmostx r_B aa_I i_I tx_I m_I o_I s_I tx_E
rahaa rahaa r_B a_I h_I aa_E
rahee rahee r_B a_I h_I ee_E
raheegaa raheegaa r_B a_I h_I ee_I g_I aa_E
rahnaa rahnaa r_B a_I h_I n_I aa_E
rakheeq rakheeq r_B a_I kh_I ee_I q_E
rakhnee rakhnee r_B a_I kh_I n_I ee_E
reekhaa reekhaa r_B ee_I kh_I aa_E
reeqj reeqj r_B ee_I q_I j_E
reesipii reesipii r_B ee_I s_I i_I p_I ii_E
rimaaiqdxar rimaaiqdxar r_B i_I m_I aa_I i_I q_I dx_I a_I r_E
rukeeqgee rukeeqgee r_B u_I k_I ee_I q_I g_I ee_E
rutx rutx r_B u_I tx_E
ruup ruup r_B uu_I p_E
saaitx saaitx s_B aa_I i_I tx_E
saamaanya saamaanya s_B aa_I m_I aa_I n_I y_I a_E
saamuuhik saamuuhik s_B aa_I m_I uu_I h_I i_I k_E
saaree saaree s_B aa_I r_I ee_E
saat saat s_B aa_I t_E
saath saath s_B aa_I th_E
saatxh saatxh s_B aa_I txh_E
sabhii sabhii s_B a_I bh_I ii_E
sabjeektxiwa sabjeektxiwa s_B a_I b_I j_I ee_I k_I tx_I i_I w_I a_E
sabsee sabsee s_B a_I b_I s_I ee_E
sacmuc sacmuc s_B a_I c_I m_I u_I c_E
sahii sahii s_B a_I h_I ii_E
sajaawatx sajaawatx s_B a_I j_I aa_I w_I a_I tx_E
sakeeq sakeeq s_B a_I k_I ee_I q_E
saksxam saksxam s_B a_I k_I sx_I a_I m_E
saktaa saktaa s_B a_I k_I t_I aa_E
saktee saktee s_B a_I k_I t_I ee_E
saktii saktii s_B a_I k_I t_I ii_E
sakuuq sakuuq s_B a_I k_I uu_I q_E
samaan samaan s_B a_I m_I aa_I n_E
samaapt samaapt s_B a_I m_I aa_I p_I t_E
samaaroh samaaroh s_B a_I m_I aa_I r_I o_I h_E
samajh samajh s_B a_I m_I a_I jh_E
samajhnee samajhnee s_B a_I m_I a_I jh_I n_I ee_E
samjhaanee samjhaanee s_B a_I m_I jh_I aa_I n_I ee_E
samuuh samuuh s_B a_I m_I uu_I h_E
samya samya s_B a_I m_I y_I a_E
saphaaii saphaaii s_B a_I ph_I aa_I ii_E
saqbaqdh saqbaqdh s_B a_I q_I b_I a_I q_I dh_E
saqbhaawit saqbhaawit s_B a_I q_I bh_I aa_I w_I i_I t_E
saqbhaawnaaeeq saqbhaawnaaeeq s_B a_I q_I bh_I aa_I w_I n_I aa_I ee_I q_E
saqbhawa saqbhawa s_B a_I q_I bh_I a_I w_I a_E
saqdarbh saqdarbh s_B a_I q_I d_I a_I r_I bh_E
saqdeeshoq saqdeeshoq s_B a_I q_I d_I ee_I sh_I o_I q_E
saqgrah saqgrah s_B a_I q_I g_I r_I a_I h_E
saqkeet saqkeet s_B a_I q_I k_I ee_I t_E
saqkhyaa saqkhyaa s_B a_I q_I kh_I y_I aa_E
saqkhyaaoq saqkhyaaoq s_B a_I q_I kh_I y_I aa_I o_I q_E
saqpatti saqpatti s_B a_I q_I p_I a_I t_I t_I i_E
saqshodhit saqshodhit s_B a_I q_I sh_I o_I dh_I i_I t_E
saqtusxtx saqtusxtx s_B a_I q_I t_I u_I sx_I tx_E
saral saral s_B a_I r_I a_I l_E
sawaal sawaal s_B a_I w_I aa_I l_E
saxrtxeedx saxrtxeedx s_B ax_I r_I tx_I ee_I dx_E
see see s_B ee_E
seel seel s_B ee_I l_E
seetx seetx s_B ee_I tx_E
shaamil shaamil sh_B aa_I m_I i_I l_E
shaayad shaayad sh_B aa_I y_I a_I d_E
shabd shabd sh_B a_I b_I d_E
shabdoq shabdoq sh_B a_I b_I d_I o_I q_E
shahar shahar sh_B a_I h_I a_I r_E
shart shart sh_B a_I r_I t_E
shaxrtxakatx shaxrtxakatx sh_B ax_I r_I tx_I a_I k_I a_I tx_E
sheesx sheesx sh_B ee_I sx_E
shiitx shiitx sh_B ii_I tx_E
shuruaat shuruaat sh_B u_I r_I u_I aa_I t_E
shuruu shuruu sh_B u_I r_I uu_E
shuunya shuunya sh_B uu_I n_I y_I a_E
sii sii s_B ii_E
siikheeqgee siikheeqgee s_B ii_I kh_I ee_I q_I g_I ee_E
siikhnee siikhnee s_B ii_I kh_I n_I ee_E
siikweeqs siikweeqs s_B ii_I k_I w_I ee_I q_I s_E
siqgal siqgal s_B i_I q_I g_I a_I l_E
siqtxeeks siqtxeeks s_B i_I q_I tx_I ee_I k_I s_E
siqtxeiks siqtxeiks s_B i_I q_I tx_I ei_I k_I s_E
siqtxeiktxik siqtxeiktxik s_B i_I q_I tx_I ei_I k_I tx_I i_I k_E
sirph sirph s_B i_I r_I ph_E
siwaaya siwaaya s_B i_I w_I aa_I y_I a_E
skawaayar skawaayar s_B k_I a_I w_I aa_I y_I a_I r_E
skuul skuul s_B k_I uu_I l_E
slaaidx slaaidx s_B l_I aa_I i_I dx_E
soc soc s_B o_I c_E
soctee soctee s_B o_I c_I t_I ee_E
soubhaagya soubhaagya s_B ou_I bh_I aa_I g_I y_I a_E
spareedx spareedx s_B p_I a_I r_I ee_I dx_E
spasxtx spasxtx s_B p_I a_I sx_I tx_E
star star s_B t_I a_I r_E
staroq staroq s_B t_I a_I r_I o_I q_E
sthaapit sthaapit s_B th_I aa_I p_I i_I t_E
stxaar stxaar s_B tx_I aa_I r_E
stxarakcar stxarakcar s_B tx_I a_I r_I a_I k_I c_I a_I r_E
stxarakcars stxarakcars s_B tx_I a_I r_I a_I k_I c_I a_I r_I s_E
stxeep stxeep s_B tx_I ee_I p_E
stxeeps stxeeps s_B tx_I ee_I p_I s_E
stxeetxmeeqtxs stxeetxmeeqtxs s_B tx_I ee_I tx_I m_I ee_I q_I tx_I s_E
stxor stxor s_B tx_I o_I r_E
sudhaar sudhaar s_B u_I dh_I aa_I r_E
sudxoku sudxoku s_B u_I dx_I o_I k_I u_E
sujhaawa sujhaawa s_B u_I jh_I aa_I w_I a_E
sunishcit sunishcit s_B u_I n_I i_I sh_I c_I i_I t_E
suuciibaddh suuciibaddh s_B uu_I c_I ii_I b_I a_I d_I dh_E
suuciyoq suuciyoq s_B uu_I c_I i_I y_I o_I q_E
suucnaaoq suucnaaoq s_B uu_I c_I n_I aa_I o_I q_E
suwidhaaoq suwidhaaoq s_B u_I w_I i_I dh_I aa_I o_I q_E
swaagat swaagat s_B w_I aa_I g_I a_I t_E
taaki taaki t_B aa_I k_I i_E
tab tab t_B a_I b_E
tabhii tabhii t_B a_I bh_I ii_E
tak tak t_B a_I k_E
tarah tarah t_B a_I r_I a_I h_E
tariikaa tariikaa t_B a_I r_I ii_I k_I aa_E
tariikee tariikee t_B a_I r_I ii_I k_I ee_E
tariikoq tariikoq t_B a_I r_I ii_I k_I o_I q_E
tark tark t_B a_I r_I k_E
tathya tathya t_B a_I th_I y_I a_E
teiyaar teiyaar t_B ei_I y_I aa_I r_E
thaa thaa th_B aa_E
thee thee th_B ee_E
thodxqaa thodxqaa th_B o_I dxq_I aa_E
thodxqee thodxqee th_B o_I dxq_I ee_E
tiin tiin t_B ii_I n_E
tirsatxh tirsatxh t_B i_I r_I s_I a_I txh_E
to to t_B o_E
tour tour t_B ou_I r_E
tulnaa tulnaa t_B u_I l_I n_I aa_E
txaaim txaaim tx_B aa_I i_I m_E
txaaip txaaip tx_B aa_I i_I p_E
txaask txaask tx_B aa_I s_I k_E
txhiik txhiik txh_B ii_I k_E
txhos txhos txh_B o_I s_E
txiim txiim tx_B ii_I m_E
txreiktxeebal txreiktxeebal tx_B r_I ei_I k_I tx_I ee_I b_I a_I l_E
txruu txruu tx_B r_I uu_E
txuu txuu tx_B uu_E
ucc ucc u_B c_I c_E
udaahranx udaahranx u_B d_I aa_I h_I r_I a_I nx_E
udxqaan udxqaan u_B dxq_I aa_I n_E
udxqaanoq udxqaanoq u_B dxq_I aa_I n_I o_I q_E
ummiid ummiid u_B m_I m_I ii_I d_E
un un u_B n_E
unheeq unheeq u_B n_I h_I ee_I q_E
unkaa unkaa u_B n_I k_I aa_E
unkee unkee u_B n_I k_I ee_E
unkii unkii u_B n_I k_I ii_E
upyog upyog u_B p_I y_I o_I g_E
us us u_B s_E
usasee usasee u_B s_I a_I s_I ee_E
usee usee u_B s_I ee_E
usii usii u_B s_I ii_E
utpaadan utpaadan u_B t_I p_I aa_I d_I a_I n_E
utxhaaee utxhaaee u_B txh_I aa_I ee_E
uupar uupar uu_B p_I a_I r_E
waaii waaii w_B aa_I ii_E
waalaa waalaa w_B aa_I l_I aa_E
waalee waalee w_B aa_I l_I ee_E
waapas waapas w_B aa_I p_I a_I s_E
waastawa waastawa w_B aa_I s_I t_I a_I w_I a_E
wah wah w_B a_I h_E
wahii wahii w_B a_I h_I ii_E
wan wan w_B a_I n_E
wanth wanth w_B a_I n_I th_E
waphaadaar waphaadaar w_B a_I ph_I aa_I d_I aa_I r_E
wardx wardx w_B a_I r_I dx_E
warnxan warnxan w_B a_I r_I nx_I a_I n_E
warnxit warnxit w_B a_I r_I nx_I i_I t_E
wartamaan wartamaan w_B a_I r_I t_I a_I m_I aa_I n_E
wartanii wartanii w_B a_I r_I t_I a_I n_I ii_E
wee wee w_B ee_E
weilyuu weilyuu w_B ei_I l_I y_I uu_E
weilyuuj weilyuuj w_B ei_I l_I y_I uu_I j_E
weilyuuoq weilyuuoq w_B ei_I l_I y_I uu_I o_I q_E
wibhinn wibhinn w_B i_I bh_I i_I n_I n_E
wicaar wicaar w_B i_I c_I aa_I r_E
wisheesx wisheesx w_B i_I sh_I ee_I sx_E
wisheesxajnjataa wisheesxajnjataa w_B i_I sh_I ee_I sx_I a_I j_I nj_I a_I t_I aa_E
wisheesxtaaeeq wisheesxtaaeeq w_B i_I sh_I ee_I sx_I t_I aa_I ee_I q_E
wishisxtx wishisxtx w_B i_I sh_I i_I sx_I tx_E
wistaar wistaar w_B i_I s_I t_I aa_I r_E
wistaarit wistaarit w_B i_I s_I t_I aa_I r_I i_I t_E
wistrqt wistrqt w_B i_I s_I t_I rq_I t_E
wiwranx wiwranx w_B i_I w_I r_I a_I nx_E
wyaakhyaan wyaakhyaan w_B y_I aa_I kh_I y_I aa_I n_E
wyakti wyakti w_B y_I a_I k_I t_I i_E
wyawasthaa wyawasthaa w_B y_I a_I w_I a_I s_I th_I aa_E
wyawasthit wyawasthit w_B y_I a_I w_I a_I s_I th_I i_I t_E
yaa yaa y_B aa_E
yaad yaad y_B aa_I d_E
yaatraa yaatraa y_B aa_I t_I r_I aa_E
yadi yadi y_B a_I d_I i_E
yah yah y_B a_I h_E
yahaamq yahaamq y_B a_I h_I aa_I mq_E
yahaaq yahaaq y_B a_I h_I aa_I q_E
yahii yahii y_B a_I h_I ii_E
yahiiq yahiiq y_B a_I h_I ii_I q_E
yee yee y_B ee_E
yogya yogya y_B o_I g_I y_I a_E
yuuniphaarm yuuniphaarm y_B uu_I n_I i_I ph_I aa_I r_I m_E
