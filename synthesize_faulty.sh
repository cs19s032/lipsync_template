
working_dir=$PWD #working_dir should have <kaldi_template> in it and wavs file should be present in audio folder and transcription should be present in transcription ( Only text )

# lang=$1  

# gender=$2

# model_path=$3

# echo "######Faulty files found. Resynthesizing. Resynth_attempt : $resynth_attempt"
# rm -r $working_dir/E2E_TTS/run_folder/*_120
# rm -r $working_dir/E2E_TTS/run_folder/*_120_denorm
# mkdir $working_dir/E2E_TTS/run_folder/test_120/
# grep -F -f  $working_dir/Faulty_files $working_dir/E2E_TTS/run_folder/test_100/text > $working_dir/E2E_TTS/run_folder/test_120/text
# #grep -F -f  $working_dir/Faulty_files $working_dir/E2E_TTS/run_folder/test_100/utt2spk > $working_dir/E2E_TTS/run_folder/test_120/utt2spk
# #grep -F -f  $working_dir/Faulty_files $working_dir/E2E_TTS/run_folder/test_100/feats.scp > $working_dir/E2E_TTS/run_folder/test_120/feats.scp

# read -p "Please change the text in $working_dir/E2E_TTS/run_folder/test_120/text and press any key to continue " word
# cd $working_dir/E2E_TTS/
# bash run_synthesis_file_multiple_resynth_split.sh $lang $gender $model_path 
# rename 's/_normalized_synthesis_new//' $working_dir/E2E_TTS/run_folder/final_120/*.wav
# rm $working_dir/E2E_TTS/run_folder/final_120/*_normalized_synthesis.wav
# mkdir $working_dir/E2E_TTS/run_folder/final_120/wav_concatenated
# for f in $(ls $working_dir/E2E_TTS/run_folder/final_120/*.wav | xargs -n 1 basename | cut -d "-" -f1 | sort | uniq); do sox $(for x in $working_dir/E2E_TTS/run_folder/final_120/$f-*.wav; do echo -n "$x $working_dir/E2E_TTS/silence_100.wav "; done) $working_dir/E2E_TTS/run_folder/final_120/wav_concatenated/$f.wav; done
# mv $working_dir/E2E_TTS/run_folder/final_120/wav_concatenated/*.wav $working_dir/audio
# mkdir $working_dir/normalized_audio
# ls $working_dir/audio/*.wav | xargs -n 1 basename > $working_dir/un_normalized_aud_list
# less $working_dir/un_normalized_aud_list | parallel "sox --norm ${working_dir}/audio/{1} ${working_dir}/normalized_audio/{1}"
# for x in $(ls ${working_dir}/normalized_audio/*.wav | xargs -n 1 basename ); do sox ${working_dir}/E2E_TTS/silence_100.wav ${working_dir}/normalized_audio/$x ${working_dir}/audio/$x; done
# #mv $working_dir/normalized_audio/*.wav $working_dir/audio/
# rm $working_dir/normalized_audio/*
# rmdir $working_dir/normalized_audio
# cd $working_dir

rm -r $working_dir/Kaldi_template/data/Tamil/train/audio
rm -r $working_dir/Kaldi_template/data/Tamil/test/audio
rm -r $working_dir/Kaldi_template/exp/*
rm -r $working_dir/Kaldi_template/mfcc/*
rm -r $working_dir/V4_template/input_data/tts_audio/*
cp -r $working_dir/audio $working_dir/Kaldi_template/data/Tamil/train/
cp -r $working_dir/audio $working_dir/Kaldi_template/data/Tamil/test/

cd $working_dir/Kaldi_template
set -e
bash run.sh $working_dir/Kaldi_template/data
set +e
bash align.sh
grep -r "No alignment" exp/mono*/log/acc.*.log | awk '{print $NF}' | sort -u | sed 's/TA_//g' > $working_dir/First_faulty
less  exp/mono/ctm | awk '{print $1}' | sort -u | sed 's/TA_//g' > $working_dir/Second_faulty
grep -Fxv -f $working_dir/Second_faulty $working_dir/First_faulty > $working_dir/Faulty_files
if [ -s $working_dir/Faulty_files ]
then
echo "=============================================================================================="
echo "Could'nt synthesize all the faulty files properly. Manual effore required. Refer Faulty_files."
echo "=============================================================================================="
else
cp $working_dir/Kaldi_template/exp/mono/ctm $working_dir/V4_template/target_ctm
cp $working_dir/audio/* $working_dir/V4_template/input_data/tts_audio/
ls $working_dir/V4_template/input_data/tts_audio/*.wav > $working_dir/V4_template/input_data/tts_aud_list
fi
