working_dir=$PWD
transcription=transcription
awk '{printf("test_100_%04d %s\n", NR, $0)}' $working_dir/${transcription} > $working_dir/transcription.txt
if [ ! -d "$working_dir/audio/*" ]; then rm -rf $working_dir/audio/*; fi
if [ ! -d "$working_dir/Kaldi_template/data/train" ]; then rm -rf $working_dir/Kaldi_template/data/train; fi
if [ ! -d "$working_dir/Kaldi_template/data/test" ]; then rm -rf $working_dir/Kaldi_template/data/test; fi


cp $working_dir/transcription.txt $working_dir/transcription_with_commas.txt

sed -i 's/\.//g' $working_dir/transcription.txt
sed -i 's/,//g' $working_dir/transcription.txt
sed -i 's/;//g' $working_dir/transcription.txt
sed -i 's/://g' $working_dir/transcription.txt
sed -i 's/!//g' $working_dir/transcription.txt
sed -i 's/?//g' $working_dir/transcription.txt
sed -i 's/-/ /g' $working_dir/transcription.txt
