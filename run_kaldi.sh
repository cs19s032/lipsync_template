##############################################################################
#										#	
#	created by mano ranjith kumar m					#	
#										#
#	date : Jul 26 2020							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#
#										#
#	Description: This code synthesizes utterances given transcription	#
#	and finds the word boundaries using Kaldi HMM-GMM ASR.		#
#										#
#	Arguments:								#
#		(1) Working Directory (Should have kaldi_template in it)	#
#		(2) Language of the synthesis					#
#		(3) Gender of the required synthesis				#
#										#
#	Requirements: The script expects, transcription (Only text) present	#
#	in the working directory. It synthesizes the transcription using	#
#	phrase breaks and aligns using kaldi HMM-GMM ASR			#
#										#		
##############################################################################


working_dir=$PWD #working_dir should have <kaldi_template> in it and wavs file should be present in audio folder and transcription should be present in transcription ( Only text )

lang=$1  

gender=$2

model_path=$3

transcription=$4
##	This section of the code creates transcription in Kaldi format	## 

awk '{printf("test_100_%04d %s\n", NR, $0)}' $working_dir/${transcription} > $working_dir/transcription.txt
if [ ! -d "$working_dir/audio/*" ]; then rm -rf $working_dir/audio/*; fi
if [ ! -d "$working_dir/Kaldi_template/data/train" ]; then rm -rf $working_dir/Kaldi_template/data/train; fi
if [ ! -d "$working_dir/Kaldi_template/data/test" ]; then rm -rf $working_dir/Kaldi_template/data/test; fi

rm -rf $working_dir/Kaldi_template/data/train/*
rm -rf $working_dir/Kaldi_template/data/test/*


cp $working_dir/transcription.txt $working_dir/transcription_with_commas.txt
##	This section of code splits the transcription using phrase breaks	##

sed -i 's/\.//g' $working_dir/transcription.txt
sed -i 's/,//g' $working_dir/transcription.txt
sed -i 's/;//g' $working_dir/transcription.txt
sed -i 's/://g' $working_dir/transcription.txt
sed -i 's/!//g' $working_dir/transcription.txt
sed -i 's/?//g' $working_dir/transcription.txt
sed -i 's/-/ /g' $working_dir/transcription.txt

bash $working_dir/phrase_breaks/phrase_text_v2.sh $working_dir/transcription.txt $working_dir/phrasified_text 2 $lang 

##	Remove symbols before synthesizing on the phrase breaked text

sed -i 's/\.//g' $working_dir/phrasified_text
sed -i 's/,//g' $working_dir/phrasified_text
sed -i 's/;//g' $working_dir/phrasified_text
sed -i 's/://g' $working_dir/phrasified_text
sed -i 's/!//g' $working_dir/phrasified_text
sed -i 's/?//g' $working_dir/phrasified_text

printf "%s " "Please verify phrasify text to fix faulty files and press any key to continue"
read ans

##	This section of the code creates lexicon				##

cp -r $working_dir/transcription.txt $working_dir/Kaldi_template/phonifier/
bash Kaldi_template/phonifier/phonify_text.sh transcription.txt lexicon.txt 25 $working_dir/Kaldi_template/phonifier
cp $working_dir/Kaldi_template/phonifier/new_text.txt $working_dir/Kaldi_template/data/Tamil/train/transcription.txt
cp $working_dir/Kaldi_template/phonifier/new_text.txt $working_dir/Kaldi_template/data/Tamil/test/transcription.txt
cat $working_dir/Kaldi_template/phonifier/lexicon.txt > $working_dir/Kaldi_template/data/local/lexicon.txt
cat $working_dir/Kaldi_template/phonifier/lexicon.txt > $working_dir/Kaldi_template/data/local/dict/lexicon.txt
echo '!SIL sil' >> $working_dir/Kaldi_template/data/local/dict/lexicon.txt
sort -u -o $working_dir/Kaldi_template/data/local/dict/lexicon.txt $working_dir/Kaldi_template/data/local/dict/lexicon.txt
mv $working_dir/transcription.txt $working_dir/transcription_without_phrasifying
mv $working_dir/phrasified_text $working_dir/transcription.txt

num_of_lines_in_transcription=$(cat "$working_dir/transcription_without_phrasifying" | wc -l)
num_of_lines_after_cls=$(cat "$working_dir/Kaldi_template/data/Tamil/train/transcription.txt"| wc -l)

echo $num_of_lines_in_transcription
echo $num_of_lines_after_cls
if [ "$num_of_lines_in_transcription" == "$num_of_lines_after_cls" ]; then
		echo "Lexicon generated Properly !!"
else
		echo "Exiting: Lexicon not generated properly. The text might contain characters that are not parsable by unified-parser (apart from symbols)."
		exit 1
fi

##	Recursive synthesis and alignment					##

resynth_attempt=1;
if [ $resynth_attempt -eq 1 ]
then
		rm $working_dir/normalized_audio/*
		rmdir $working_dir/normalized_audio
		echo python3 IITM_TTS_API_FS2/IITM_TTS_API.py $transcription $working_dir/normalized_audio $lang $gender 
		python3 IITM_TTS_API_FS2/IITM_TTS_API.py $transcription $working_dir/normalized_audio $lang $gender 
		ls $working_dir/audio/*.wav | xargs -n 1 basename > $working_dir/un_normalized_aud_list
		less $working_dir/un_normalized_aud_list | parallel "sox --norm ${working_dir}/audio/{1} ${working_dir}/normalized_audio/{1}"
		mv $working_dir/normalized_audio/*.wav $working_dir/audio/
		rm $working_dir/normalized_audio/*
		rmdir $working_dir/normalized_audio
fi       
##	First Synthesis done

##	Aligning with kaldi and resynthesizing if not properly synthesized thrice

rm -r $working_dir/Kaldi_template/data/Tamil/train/audio
rm -r $working_dir/Kaldi_template/data/Tamil/test/audio
rm -r $working_dir/Kaldi_template/exp/*
rm -r $working_dir/Kaldi_template/mfcc/*
cp -r $working_dir/audio $working_dir/Kaldi_template/data/Tamil/train/
cp -r $working_dir/audio $working_dir/Kaldi_template/data/Tamil/test/

cd $working_dir/Kaldi_template
set -e
bash run.sh $working_dir/Kaldi_template/data
set +e
bash align.sh
grep -r "No alignment" exp/mono*/log/acc.*.log | awk '{print $NF}' | sort -u | sed 's/TA_//g' > $working_dir/First_faulty
less  exp/mono/ctm | awk '{print $1}' | sort -u | sed 's/TA_//g' > $working_dir/Second_faulty
grep -Fxv -f $working_dir/Second_faulty $working_dir/First_faulty > $working_dir/Faulty_files

###### Checking if resynthesis is required #####################

if [ -s $working_dir/Faulty_files ]
then
		echo "----------------------------------------------------------------------------------------------"
		echo "Could'nt synthesize all the faulty files properly. Manual effore required. Refer Faulty_files."
		echo "----------------------------------------------------------------------------------------------"
		mv $working_dir/transcription.txt $working_dir/phrasified_text
		mv $working_dir/transcription_without_phrasifying $working_dir/transcription.txt
		exit 1
fi

echo "synthesis ended"

cp $working_dir/Kaldi_template/exp/mono/ctm $working_dir/V4_template/target_ctm
cp $working_dir/audio/* $working_dir/V4_template/input_data/tts_audio/
ls $working_dir/V4_template/input_data/tts_audio/*.wav > $working_dir/V4_template/input_data/tts_aud_list
mv $working_dir/transcription.txt $working_dir/phrasified_text
mv $working_dir/transcription_without_phrasifying $working_dir/transcription.txt

exit 0


