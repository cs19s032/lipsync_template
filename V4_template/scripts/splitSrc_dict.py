#!/usr/bin/python3

# Split source mp4 video based on source srt file and verify if the number of segments match with tts audio list
# Author: Jom Kuriakose, Mano Ranjith Kumar M
# email: jom@cse.iitm.ac.in, manoranjith@smail.iitm.ac.in
# Date: 09/05/2022


# Import packages
import sys
import argparse
import pysrt
from moviepy.editor import *

# Print comment header
"""
def code_head_print():
	code_head_str='Split source mp4 video based on source srt file and verify if the number of segments match with tts audio list\n\n'
	code_head_str+='Author: Jom Kuriakose\n, Mano Ranjith Kumar M'
	code_head_str+='email: jom@cse.iitm.ac.in\n, manoranjith@smail.iitm.ac.in'
	code_head_str+='Date: 22/01/2021\n\n'
	return code_head_str

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: python splitSrc.py -v <src-video-file> -n <src-video-nosound-file> -s <src-srt-file> -t <tts-audio-list-file> -o <output-srt-video-list-file> -f <output-full-video-list-file> -l <output-video-seg-loc> -k <skip-srt-flag>\n'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-v','--src_vid', help='Source video file (mp4 format).', required=True)
	parser.add_argument('-n','--src_vid_no', help='Source video file without sound (mp4 format).', required=True)
	parser.add_argument('-s','--src_srt', help='Source .srt file.', required=True)
	parser.add_argument('-t','--tts_lst', help='TTS audio file list.', required=True)
	parser.add_argument('-o','--vid_lst', help='Output list of video segments at srt entries.', required=True)
	parser.add_argument('-f','--full_lst', help='Output list of all video segments including between srt entries.', required=True)
	parser.add_argument('-l','--out_loc', help='Location of output video segments.', required=True)
	parser.add_argument('-k','--skip_flag', help='Skip the first srt entry - for NPTEL lectures (default=0 -- not skip).\n\n', required=False, default=0)
	args = parser.parse_args()
	return args
"""
class splitSrc(object):
	def __init__(self, src_vid, src_vid_no,src_srt,tts_lst,vid_lst,full_lst,out_loc,skip_flag_val):
		self.src_vid=src_vid
		self.src_vid_no=src_vid_no
		self.src_srt=src_srt
		self.tts_lst=tts_lst
		self.vid_lst=vid_lst
		self.full_lst=full_lst
		self.out_loc=out_loc
		self.skip_flag=skip_flag_val
	
	def main(self):
		src_vid=self.src_vid
		src_vid_no=self.src_vid_no
		src_srt=self.src_srt
		tts_lst=self.tts_lst
		out_lst=self.vid_lst
		full_lst=self.full_lst
		out_loc=self.out_loc
		skip_flag=self.skip_flag
		dict={}
		# Read videos and files
		src_video = VideoFileClip(src_vid)
		src_nosound_video = VideoFileClip(src_vid_no)
		src_sub = pysrt.open(src_srt, encoding='utf-8')
		with open(tts_lst) as f:
			tts_list_temp = f.read()
		tts_list=tts_list_temp.strip().split('\n')
		out_list=open(out_lst,'w')
		full_list=open(full_lst,'w')

		if(skip_flag==1):
			if(len(tts_list) == (len(src_sub)-1)):
				print("Number of srt entries and tts outputs match.")
			else:
				print("Error: Number of srt entries and tts outputs doesn't match")
				print(len(tts_list))
				print(len(src_sub)-1)
				sys.exit()
		else:
			if(len(tts_list) == len(src_sub)):
				print("Number of srt entries and tts outputs match.")
			else:
				print("Error: Number of srt entries and tts outputs doesn't match")
				print(len(tts_list))
				print(len(src_sub)-1)
				sys.exit()

		start_time = 0.0
		for i in range(len(src_sub)):
			print("Splitting segment"+str(i))
			if(skip_flag==1):
				if(i == 0):
					continue
				elif(i == 1):
					end_time=(((src_sub[i].start.hours * 60 + src_sub[i].start.minutes) * 60 + src_sub[i].start.seconds) *1000 + src_sub[i].start.milliseconds)/1000
					out_srt_name=out_loc+'/srt_seg'+str(i).zfill(4)+'.mp4'
					if(end_time-start_time>1):
						intervidclip=src_video.subclip(start_time,end_time)
						out_inter_name=out_loc+'/intersrt_seg'+str(i).zfill(4)+'.mp4'
						full_list.write(out_inter_name+'\n')
						video_final = concatenate_videoclips([intervidclip])
						dict[out_inter_name]=video_final
						#video_final.write_videofile(out_inter_name)
					start_time=(((src_sub[i].end.hours * 60 + src_sub[i].end.minutes) * 60 + src_sub[i].end.seconds) *1000 + src_sub[i].end.milliseconds)/1000
					srtvidclip=src_video.subclip(end_time,start_time)
					out_list.write(out_srt_name+'\n')
					full_list.write(out_srt_name+'\n')
					video_final = concatenate_videoclips([srtvidclip])
					dict[out_srt_name]=video_final
					#video_final.write_videofile(out_srt_name)
				else:
					end_time=(((src_sub[i].start.hours * 60 + src_sub[i].start.minutes) * 60 + src_sub[i].start.seconds) *1000 + src_sub[i].start.milliseconds)/1000
					out_srt_name=out_loc+'/srt_seg'+str(i).zfill(4)+'.mp4'
					if(end_time-start_time>1): # Checks if the inter srt segments are bigger than 1 sec
						intervidclip=src_nosound_video.subclip(start_time,end_time)
						out_inter_name=out_loc+'/intersrt_seg'+str(i).zfill(4)+'.mp4'
						full_list.write(out_inter_name+'\n')
						video_final = concatenate_videoclips([intervidclip])
						dict[out_inter_name]=intervidclip
						#video_final.write_videofile(out_inter_name)
					start_time=(((src_sub[i].end.hours * 60 + src_sub[i].end.minutes) * 60 + src_sub[i].end.seconds) *1000 + src_sub[i].end.milliseconds)/1000
					srtvidclip=src_video.subclip(end_time,start_time)
					out_list.write(out_srt_name+'\n')
					full_list.write(out_srt_name+'\n')
					video_final = concatenate_videoclips([srtvidclip])
					dict[out_srt_name]=video_final
					#video_final.write_videofile(out_srt_name)
			else:
				end_time=(((src_sub[i].start.hours * 60 + src_sub[i].start.minutes) * 60 + src_sub[i].start.seconds) *1000 + src_sub[i].start.milliseconds)/1000
				out_srt_name=out_loc+'/srt_seg'+str(i).zfill(4)+'.mp4'
				if(end_time-start_time>1): # Checks if the inter srt segments are bigger than 1 sec
					intervidclip=src_nosound_video.subclip(start_time,end_time)
					out_inter_name=out_loc+'/intersrt_seg'+str(i).zfill(4)+'.mp4'
					full_list.write(out_inter_name+'\n')
					video_final = concatenate_videoclips([intervidclip])
					dict[out_inter_name]=video_final
					#video_final.write_videofile(out_inter_name)
				start_time=(((src_sub[i].end.hours * 60 + src_sub[i].end.minutes) * 60 + src_sub[i].end.seconds) *1000 + src_sub[i].end.milliseconds)/1000
				srtvidclip=src_video.subclip(end_time,start_time)
				out_list.write(out_srt_name+'\n')
				full_list.write(out_srt_name+'\n')
				video_final = concatenate_videoclips([srtvidclip])
				dict[out_srt_name]=video_final
				#video_final.write_videofile(out_srt_name)

		out_list.close()
		full_list.close()
		return dict

	if __name__ == "__main__":
		main()

