#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#

from pathlib import Path
from shutil import copyfile
import sys,csv
import os

def closest(lst, K):
      
    return lst[min(range(len(lst)), key = lambda i: abs(lst[i]-K))]

ctm_file = sys.argv[1]
ste_file = sys.argv[2]
mylabfile = Path(ste_file)
target_dir = sys.argv[3]
Path(target_dir).mkdir(parents=True, exist_ok=True)
if mylabfile.is_file():
   print("Fonund ste file correcting boundaries"+ste_file)
else:
   print("Ste file named "+ ste_file +" is missing")
with open(ste_file) as f:
    ste_values = f.read().splitlines()

size = os.path.getsize(ctm_file)       
with open(ctm_file,"r") as fp:
   line = fp.readline()
   size -= len(line)
   num_lines = sum(1 for line in open(ctm_file))
   if(num_lines == 1):
       line = line.strip()
       columns = line.split()
       copyfile(ctm_file,target_dir+'/'+columns[0]+'.stxt' )
   curr_start_value = 0
   curr_label = ""
   curr_duration = 0
   written_flag = 0
   while line:
       line = line.strip()
       columns = line.split()
       start_value = float(columns[2])
       label = str(columns[4])
       duration = float(columns[3])
       end_value = float(columns[3]) + float(columns[2])
       index = int(end_value * 22050)-1
       #print(index)
       #print(ste_values)
       if(index > len(ste_values)):
          index = len(ste_values)-1
       ene_value = float(ste_values[index])
       if(ene_value > 0.05):
          if(curr_start_value == 0):
             curr_start_value = start_value
          curr_label  = curr_label + label
          curr_duration = curr_duration + duration
       else:
         with open(target_dir+'/'+columns[0]+'.stxt', 'a+') as the_file:
            if(curr_label != "" and curr_start_value != 0 and curr_duration != 0):
               written_flag=1
               columns[2] = curr_start_value
               columns[3] = curr_duration + duration
               columns[3] = columns[3]+(start_value-(curr_start_value+curr_duration)) 
               columns[4] = curr_label + label
               curr_start_value = 0
               curr_label  = ""
               curr_duration = 0
            line = " ".join([str(i) for i in columns])
            the_file.write(line+"\n")
       if(not size and written_flag==0 and not Path(target_dir+'/'+columns[0]+'.stxt').is_file()):
         with open(target_dir+'/'+columns[0]+'.stxt', 'a+') as the_file:
            columns[2] = curr_start_value
            columns[3] = curr_duration
            columns[4] = curr_label
            curr_start_value = 0
            curr_label  = ""
            curr_duration = 0
            line = " ".join([str(i) for i in columns])
            the_file.write(line+"\n")
       line = fp.readline()
       size -= len(line)
