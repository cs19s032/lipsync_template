#!/usr/bin/python3

# Script to align source srt and tts output
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 19/01/2021

# Import packages
import os
import sys
import argparse
from moviepy.editor import *

# Print comment header
def code_head_print():
	code_head_str='Script to align source srt and tts output\n\n'
	code_head_str+='Author: Mano Ranjith Kumar M\n'
	code_head_str+='email: mano1381997@gmail.com\n'
	code_head_str+='Date: 9/03/2021\n\n'
	return code_head_str

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: python align-seg-v2.py -s <srt_boundary_file> -t <tts_boundary_file> -v <src-video-without-sound> -a <tts-audio-file> -o <ouput-video> -l <output-seg-list> -d <output-segs-folder> -f <filename>\n'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-s','--source_boundary_filename', help='SRT boundary filename', required=True)
	parser.add_argument('-t','--tts_boundary_filename', help='TTS boundary filename', required=True)
	parser.add_argument('-v','--source_nosound_video_file', help='Source video file without sound', required=True)
	parser.add_argument('-a','--tts_audio_file', help='TTS audio file', required=True)
	parser.add_argument('-l','--output_segs_folder_list', help='List of folders to store intra srt videos', required=True)
	parser.add_argument('-d','--output_segs_folder', help='Folder to store output of intra srt video segments and srt videos', required=True)
	parser.add_argument('-f','--filename', help='filename\n\n', required=True)
	args = parser.parse_args()
	return args

def read_boundary_file(fileName):
	with open(fileName) as f:
		data_temp1 = f.read() #data_temp1 - read raw data from file
	data_temp2 = data_temp1.split('\n') #data_temp2 - split the raw data into lines
	data_temp3 = [] #data_temp3 - strip and split lines further into lists
	for n in data_temp2:
		if n != '':
			data_temp3.append(n.strip().split(' '))
	data_temp4 = [] #data_temp4 - start_time <space> end_time <space> duration <space> speech(1)/silence(-1) flag
	for i in range(len(data_temp3)):
		if (i==0):
			start_time=0.0
			label=int(data_temp3[i][2])
		else:
			if (i % 2)==0:
				start_time=float(data_temp3[i][1])
				label=int(data_temp3[i][2])
			else:
				if (label == int(data_temp3[i][2])):
					end_time=float(data_temp3[i][1])
					duration=end_time-start_time
					data_temp4.append([start_time,end_time,duration,label])
				else:
					print('Issue with silence labels')
					sys.exit()
	return data_temp4

def make_info_lists(data_temp4):
	data_temp5=[] #data_temp5 - duration of speech segments
	data_temp6=[] #data_temp6 - index of speech segments
	data_temp7=[] #data_temp7 - duration of silence segments
	data_temp8=[] #data_temp8 - index of silence segments
	for i in range(len(data_temp4)):
		if(data_temp4[i][3]==1):
			data_temp5.append(data_temp4[i][2])
			data_temp6.append(i)
		else:
			data_temp7.append(data_temp4[i][2])
			data_temp8.append(i)
	data={} #data - dictionary of all relevant variables
	data['speech_dur_list']=data_temp5
	data['speech_idx_list']=data_temp6
	data['sil_dur_list']=data_temp7
	data['sil_idx_list']=data_temp8
	return data

def merge_sil(data_temp1):
	for i in range(len(data_temp1)):
		if (i==0) and (data_temp1[i][3]==-1):
			data_temp1[i+1][0]=data_temp1[i][0]
		elif (i==len(data_temp1)-1) and (data_temp1[i][3]==-1):
			data_temp1[i-1][1]=data_temp1[i][1]
		elif(data_temp1[i][3]==-1):
			data_temp1[i-1][1]=data_temp1[i-1][1]+(data_temp1[i][2]/2)
			data_temp1[i+1][0]=data_temp1[i+1][0]-(data_temp1[i][2]/2)
		else:
			pass

	data_temp2=[] #data_temp2 - corrected alignments
	tts_align_time=[] 
	data_temp3=[] #data_temp3 - corrected durations
	tts_speech_dur_list = []
	for i in range(len(data_temp1)):
		if(data_temp1[i][3]==1):
			data_temp1[i][2]=data_temp1[i][1]-data_temp1[i][0]
			data_temp2.append(data_temp1[i])
			data_temp3.append(data_temp1[i][2])
	
	output_data={}
	output_data['time_align_list']=data_temp2
	output_data['speech_dur_list']=data_temp3
	return output_data

def percent_dur(dur_list):
	dur_per = []
	dur_full = sum(dur_list)
	for i in range(len(dur_list)):
		dur_per.append(sum(dur_list[:i+1])/dur_full*100)
	return dur_per[:-1]

def sort_dur(dur_list,direction):
	dur_sort_idx_list = sorted(range(len(dur_list)), key=lambda k: dur_list[k], reverse=direction)
	return dur_sort_idx_list

def align_srt(src_data,synth_data,top):
	src_info_list = make_info_lists(src_data)
	src_sil_dur_list = src_info_list['sil_dur_list']
	src_sil_idx_list = src_info_list['sil_idx_list']
	src_speech_dur_list = src_info_list['speech_dur_list']
	src_speech_idx_list = src_info_list['speech_idx_list']
	src_speech_dur_percent = percent_dur(src_speech_dur_list)
	tts_info_list = make_info_lists(synth_data)
	tts_speech_dur_list = tts_info_list['speech_dur_list']
	tts_speech_idx_list = tts_info_list['speech_idx_list']
	tts_speech_dur_percent = percent_dur(tts_speech_dur_list)
	if len(src_data) == 1 and len(synth_data) == 1:
		return [[[1],src_data[0],synth_data[0]]]
	elif len(src_data) == 1:
		return [[[1],src_data[0],[synth_data[0][0],synth_data[len(synth_data)-1][1],synth_data[len(synth_data)-1][1]-synth_data[0][0],1]]]
	elif len(synth_data) == 1:
		return [[[1],[src_data[0][0],src_data[len(src_data)-1][1],src_data[len(src_data)-1][1]-src_data[0][0],1],synth_data[0]]]
	else:
		sil_idx_list = sort_dur(src_sil_dur_list,True)
		sil_idx = sil_idx_list[top]
		cut_idx = src_sil_idx_list[sil_idx]
		if (cut_idx == len(src_data)-1):
			return align_srt(src_data[:cut_idx],synth_data,0) + [[[-1],src_data[cut_idx],[]]]
		elif (cut_idx==0):
			return [[[-1],src_data[cut_idx],[]]] + align_srt(src_data[cut_idx+1:],synth_data,0)
		else:
			cut_percent = src_speech_dur_percent[src_speech_idx_list.index(cut_idx-1)]
			tts_near = min(enumerate(tts_speech_dur_percent), key=lambda x: abs(x[1]-cut_percent))
			if abs(cut_percent-tts_near[1]) > 10:
				if top+1<len(sil_idx_list):
					return align_srt(src_data,synth_data,top+1)
				else:
					return [[[1],[src_data[0][0],src_data[len(src_data)-1][1],src_data[len(src_data)-1][1]-src_data[0][0],1],[synth_data[0][0],synth_data[len(synth_data)-1][1],synth_data[len(synth_data)-1][1]-synth_data[0][0],1]]]
			else:
				tts_idx = tts_near[0]+1
				return align_srt(src_data[:cut_idx],synth_data[:tts_idx],0) + [[[-1],src_data[cut_idx],[]]] + align_srt(src_data[cut_idx+1:],synth_data[tts_idx:],0)

def merge_src_sil(data_temp1):
	max_dur=0.5 # seconds
	sil_dur_list=[]
	sil_dur_idx=[]
	for i in range(len(data_temp1)):
		if (data_temp1[i][3]==-1):
			sil_dur_list.append(data_temp1[i][1]-data_temp1[i][0])
			sil_dur_idx.append(i)

	if (len(sil_dur_list)<=0):
		return data_temp1

	sil_idx_list = sort_dur(sil_dur_list,False)
	sil_idx = sil_idx_list[0]
	dur = sil_dur_list[sil_idx]
	dur_idx = sil_dur_idx[sil_idx]
	while(True):
		if (dur < max_dur):
			if (dur_idx == 0):
				data_temp1[dur_idx+1][0] = data_temp1[dur_idx][0]
				data_temp1[dur_idx+1][2] = data_temp1[dur_idx+1][1] - data_temp1[dur_idx+1][0]
				del(data_temp1[dur_idx])
			elif (dur_idx == len(data_temp1)-1):
				data_temp1[dur_idx-1][1] = data_temp1[dur_idx][1]
				data_temp1[dur_idx-1][2] = data_temp1[dur_idx-1][1] - data_temp1[dur_idx-1][0]
				del(data_temp1[dur_idx])
			else:
				data_temp1[dur_idx-1][1] = data_temp1[dur_idx+1][1]
				data_temp1[dur_idx-1][2] = data_temp1[dur_idx-1][1] - data_temp1[dur_idx-1][0]
				del(data_temp1[dur_idx:dur_idx+2])
			sil_dur_list=[]
			sil_dur_idx=[]
			for i in range(len(data_temp1)):
				if (data_temp1[i][3]==-1):
					sil_dur_list.append(data_temp1[i][1]-data_temp1[i][0])
					sil_dur_idx.append(i)

			if (len(sil_dur_list)<=0):
				return data_temp1

			sil_idx_list = sort_dur(sil_dur_list,False)
			sil_idx = sil_idx_list[0]
			dur = sil_dur_list[sil_idx]
			dur_idx = sil_dur_idx[sil_idx]
		else:
			break
	return data_temp1


def main():
	args=parse_args()
	src_align_file_name=args.source_boundary_filename
	tts_align_file_name=args.tts_boundary_filename
	source_nosound_video_file=args.source_nosound_video_file
	tts_audio_file=args.tts_audio_file
	output_segs_folder=args.output_segs_folder
	output_segs_folder_list=args.output_segs_folder_list
	filename=args.filename

	src_align_list=read_boundary_file(src_align_file_name)
	#src_align_time_list=read_boundary_file(src_align_file_name)
	src_align_time_list=merge_src_sil(src_align_list)
	tts_align_list=read_boundary_file(tts_align_file_name)
	tts_sil_merge_data=merge_sil(tts_align_list)
	tts_align_time_list = tts_sil_merge_data['time_align_list']

	# Recurssive code - checks the longest silence down to the shortest
	alignment = align_srt(src_align_time_list,tts_align_time_list,0)

	# Load video and audio files
	video_nosound = VideoFileClip(source_nosound_video_file)
	audio = AudioFileClip(tts_audio_file)

	if not os.path.exists(output_segs_folder+'/'+filename):
		os.makedirs(output_segs_folder+'/'+filename)
	f = open(output_segs_folder+'/'+filename+'/'+filename+'_list.txt','w')
	g = open(output_segs_folder_list,'a+')
	g.write(output_segs_folder+'/'+filename+'\n')
	g.close()
	video_list=[]
	
	for i in range(len(alignment)):
		if(alignment[i][0][0] == -1):
			temp_clip = video_nosound.subclip(alignment[i][1][0], alignment[i][1][1])
			outFileName=output_segs_folder+'/'+filename+'/'+filename+'_seg_'+str(i)+'.mp4'
			f.write("file '"+filename+"_seg_"+str(i)+".mp4\n")
			video_final = concatenate_videoclips([temp_clip])
			#video_final.write_videofile(outFileName)
			video_list.append(temp_clip)
		elif(alignment[i][0][0] == 1):
			video_clip = video_nosound.subclip(alignment[i][1][0], alignment[i][1][1])
			audio_clip = audio.subclip(alignment[i][2][0], alignment[i][2][1])
			video_warp_ratio = (alignment[i][1][1]-alignment[i][1][0])/(alignment[i][2][1]-alignment[i][2][0])
			warp_video = video_clip.fx(vfx.speedx, video_warp_ratio)
			warp_video.audio = audio_clip
			outFileName=output_segs_folder+'/'+filename+'/'+filename+'_seg_'+str(i)+'.mp4'
			f.write("file '"+filename+"_seg_"+str(i)+".mp4\n")
			video_final = concatenate_videoclips([warp_video])
			#video_final.write_videofile(outFileName)
			video_list.append(warp_video)
		else:
			print('Error!!')
			sys.exit()
	
	video_final = concatenate_videoclips(video_list)
	video_final.write_videofile('final_videos/'+filename+'.mp4')
	f.close()

# Main function
if __name__ == "__main__":
    main()

