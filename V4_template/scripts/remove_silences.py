#!/usr/bin/python3

import os
import sys
import argparse
from pydub import AudioSegment

# Print comment header
def code_head_print():
	code_head_str='Script to remove beginning and ending silence from TTS output\n\n'
	code_head_str+='Author: Mano & Jom\n'
	code_head_str+='email: jom@cse.iitm.ac.in\n'
	code_head_str+='Date: 02/02/2021\n\n'
	return code_head_str

# Print example command
def ex_cmd_print():
	ex_cmd_str='\nExample: python remove_silences.py -i <input-wav-file> -o <output-wav-file>\n'
	return ex_cmd_str

# Parse arguments and print details
def parse_args():
	parser = argparse.ArgumentParser(description=code_head_print()+ex_cmd_print(),formatter_class=argparse.RawTextHelpFormatter)
	parser.add_argument('-i','--input', help='Input wavfile', required=True)
	parser.add_argument('-o','--output', help='Output wavfile\n\n', required=True)
	args = parser.parse_args()
	return args

def detect_leading_silence(sound, silence_threshold=-30.0, chunk_size=10):
    '''
    sound is a pydub.AudioSegment
    silence_threshold in dB
    chunk_size in ms

    iterate over chunks until you find the first one with sound
    '''
    trim_ms = 0 # ms

    assert chunk_size > 0 # to avoid infinite loop
    while sound[trim_ms:trim_ms+chunk_size].dBFS < silence_threshold and trim_ms < len(sound):
        trim_ms += chunk_size

    return trim_ms

def main():
	args=parse_args()
	source_sound=args.input
	dest_sound=args.output

	sound = AudioSegment.from_file(source_sound, format="wav")
	start_trim = detect_leading_silence(sound)
	end_trim = detect_leading_silence(sound.reverse())
	duration = len(sound)
	if ((duration-end_trim)-start_trim) > 0:
		trimmed_sound = sound[start_trim:duration-end_trim]
	else:
		trimmed_sound = sound
	trimmed_sound.export(dest_sound, format="wav")

# Main function
if __name__ == "__main__":
	main()

