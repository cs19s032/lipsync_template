#!/usr/bin/python3

# Script to align source srt and tts output
# Author: Jom Kuriakose, Mano Ranjith Kumar 
# email: jom@cse.iitm.ac.in, manoranjith@smail.iitm.ac.in
# Date: 19/01/2021

# Import packages
import os
import sys
import argparse
from moviepy.editor import *
import moviepy.audio.fx.all as afx
import fcntl


class align_seg(object):
	def __init__(self,source_boundary_filename,tts_boundary_filename,source_nosound_video_file,tts_audio_file,filename,final_videos_dir):
		self.src_align_file_name=source_boundary_filename
		self.tts_align_file_name=tts_boundary_filename
		self.source_nosound_video_file=source_nosound_video_file
		self.tts_audio_file=tts_audio_file
		self.filename=filename
		self.final_videos_dir = final_videos_dir
	
	def read_boundary_file(self,fileName):
		with open(fileName) as f:
			data_temp1 = f.read() #data_temp1 - read raw data from file
		data_temp2 = data_temp1.split('\n') #data_temp2 - split the raw data into lines
		data_temp3 = [] #data_temp3 - strip and split lines further into lists
		for n in data_temp2:
			if n != '':
				data_temp3.append(n.strip().split(' '))
		data_temp4 = [] #data_temp4 - start_time <space> end_time <space> duration <space> speech(1)/silence(-1) flag
		for i in range(len(data_temp3)):
			if (i==0):
				start_time=0.0
				label=int(data_temp3[i][2])
			else:
				if (i % 2) ==0:
					start_time=float(data_temp3[i][1])
					label=int(data_temp3[i][2])
				else:
					if (label == int(data_temp3[i][2])):
						end_time=float(data_temp3[i][1])
						duration=end_time-start_time
						data_temp4.append([start_time,end_time,duration,label])
					else:
						print('Issue with silence labels')
						sys.exit()
		return data_temp4
		
	def read_ctm_boundary_file(self,fileName):
		with open(fileName) as f:
			data_temp1 = f.read() #data_temp1 - read raw data from file
		data_temp2 = data_temp1.split('\n') #data_temp2 - split the raw data into lines
		data_temp3 = [] #data_temp3 - strip and split lines further into lists
		for n in data_temp2:
			if n != '':
				data_temp3.append(n.strip().split(' '))
		data_temp4 = [] #data_temp4 - start_time <space> end_time <space> duration <space> speech(1)/silence(-1) flag
		#print(data_temp3)
		for i in range(len(data_temp3)):
			data_temp3[i][3] = float(data_temp3[i][2])+float(data_temp3[i][3])
		for i in range(len(data_temp3)):
			if (i==0):
				start_time=float(data_temp3[i][2])
				end_time = float(data_temp3[i][3])
				duration = end_time - start_time
				label = 1
				if( "SIL" in data_temp3[i][4]):
					label = -1
				data_temp4.append([start_time,end_time,duration,label,data_temp3[i][4]])
			
			else:
				if(float(data_temp3[i-1][3]) == float(data_temp3[i][2])):
					start_time=float(data_temp3[i][2])
				else:
					start_time =float(data_temp3[i-1][3])
					end_time = float(data_temp3[i][2])
					duration = end_time - start_time
					label = -1 
					data_temp4.append([start_time,end_time,duration,label,"SIL"])
					start_time = float(data_temp3[i][2])			
				label = 1
				if( "SIL" in data_temp3[i][4]):
					label = -1
				end_time =  float(data_temp3[i][3])
				duration =  end_time - start_time
				data_temp4.append([start_time,end_time,duration,label,data_temp3[i][4]])
		#print(data_temp4)
		return data_temp4		


	def tts_make_info_lists(self,data_temp4):
		data_temp5=[] #data_temp5 - duration of speech segments
		data_temp6=[] #data_temp6 - index of speech segments
		data_temp7=[] #data_temp7 - duration of silence segments
		data_temp8=[] #data_temp8 - index of silence segments
		data_temp9=[] #data_temp9 - label of speech segments
		for i in range(len(data_temp4)):
			if(data_temp4[i][3]==1):
				data_temp5.append(data_temp4[i][2])
				data_temp6.append(i)
				data_temp9.append(data_temp4[i][4])
			else:
				data_temp7.append(data_temp4[i][2])
				data_temp8.append(i)
		data={} #data - dictionary of all relevant variables
		data['speech_dur_list']=data_temp5
		data['speech_idx_list']=data_temp6
		data['sil_dur_list']=data_temp7
		data['sil_idx_list']=data_temp8
		data['speech_label']=data_temp9
		return data
		
	def make_info_lists(self,data_temp4):
		data_temp5=[] #data_temp5 - duration of speech segments
		data_temp6=[] #data_temp6 - index of speech segments
		data_temp7=[] #data_temp7 - duration of silence segments
		data_temp8=[] #data_temp8 - index of silence segments
		for i in range(len(data_temp4)):
			if(data_temp4[i][3]==1):
				data_temp5.append(data_temp4[i][2])
				data_temp6.append(i)
			else:
				data_temp7.append(data_temp4[i][2])
				data_temp8.append(i)
		data={} #data - dictionary of all relevant variables
		data['speech_dur_list']=data_temp5
		data['speech_idx_list']=data_temp6
		data['sil_dur_list']=data_temp7
		data['sil_idx_list']=data_temp8
		return data

	def merge_sil(self,data_temp1):
		for i in range(len(data_temp1)):
			print(i)
			print(data_temp1[i]) 
			if (i==0) and (data_temp1[i][3]==-1): #If the first element is sil, it removes it
				data_temp1[i+1][0]=data_temp1[i][0]
			elif (i==len(data_temp1)-1) and (data_temp1[i][3]==-1): #if the last element is sil,it removes it
				data_temp1[i-1][1]=data_temp1[i][1]
			elif(data_temp1[i][3]==-1 and data_temp1[i][2] < 0.10 ): #if there is any other small sil(less than 100ms), it splits into half and add's it to before and after sil
				data_temp1[i-1][1]=data_temp1[i-1][1]+(data_temp1[i][2]/2)
				data_temp1[i+1][0]=data_temp1[i+1][0]-(data_temp1[i][2]/2)
			elif(data_temp1[i][3]==-1): #if there is any other sil it can be ignored as phrase break is being handled
				del(data_temp1[i])
			else:
				pass
			if(i+1>=len(data_temp1)): #this condition is added to handle the list shrinkage of the for loop when items are being deleted
				break
		data_temp2=[] #data_temp2 - corrected alignments
		tts_align_time=[] 
		data_temp3=[] #data_temp3 - corrected durations
		tts_speech_dur_list = []
		for i in range(len(data_temp1)):
			if(data_temp1[i][3]==1):
				data_temp1[i][2]=data_temp1[i][1]-data_temp1[i][0]
				data_temp2.append(data_temp1[i])
				data_temp3.append(data_temp1[i][2])
		
		output_data={}
		output_data['time_align_list']=data_temp2
		output_data['speech_dur_list']=data_temp3
		print(output_data)
		return output_data

	def percent_dur(self,dur_list):
		dur_per = []
		dur_full = sum(dur_list)
		for i in range(len(dur_list)):
			dur_per.append(sum(dur_list[:i+1])/dur_full*100)
		return dur_per[:-1]

	def sort_dur(self,dur_list,direction):
		dur_sort_idx_list = sorted(range(len(dur_list)), key=lambda k: dur_list[k], reverse=direction)
		return dur_sort_idx_list

	def isFuncWordThere(self,word, function_words,count):
		print("checking for nearby breaks :",count)
		count-=word.count("*")
		count-=word.count("<B>")
		for i in range(len(function_words)):
			if(word.endswith(function_words[i]) and count >= 0 ):
				return True,count
		return False,count
		
		
	""" Deprecated
	def whatFuncWordThere(self,word, function_words):
		for i in range(len(function_words)):
			if(word.endswith(function_words[i])):
				return function_words[i]
	"""	
	
	def align_srt(self,src_data,synth_data,top,filename):
		function_words=["<B>"]
		src_info_list = self.make_info_lists(src_data)
		src_sil_dur_list = src_info_list['sil_dur_list'] #From the source list, duration of silences (marked by -1)
		src_sil_idx_list = src_info_list['sil_idx_list'] #corresponding indexes of silences
		src_speech_dur_list = src_info_list['speech_dur_list'] #The remaining segments from source list. This list contains the duration of speech regions
		src_speech_idx_list = src_info_list['speech_idx_list'] #corresponding indexes of speech regions
		src_speech_dur_percent = self.percent_dur(src_speech_dur_list) #Percentage of each speech region compared to the whole audio
		tts_info_list = self.tts_make_info_lists(synth_data)
		tts_speech_dur_list = tts_info_list['speech_dur_list'] #speech regions from the tts speech duration list (Not sorted)
		tts_speech_idx_list = tts_info_list['speech_idx_list'] #indexes of the same using tts speech duration list (Not sorted)
		tts_speech_label_list = tts_info_list['speech_label'] #speech label of the same
		tts_speech_dur_percent = self.percent_dur(tts_speech_dur_list) #duration of the same
		threshold_difference = 20 #Tolerable_in_percentage
		#print()
		#print(tts_speech_label_list)
		if len(src_data) == 1 and len(synth_data) == 1:
			return [[[1],src_data[0],synth_data[0]]]
		elif len(src_data) == 1:
			return [[[1],src_data[0],[synth_data[0][0],synth_data[len(synth_data)-1][1],synth_data[len(synth_data)-1][1]-synth_data[0][0],1]]]
		elif len(synth_data) == 1:
			return [[[1],[src_data[0][0],src_data[len(src_data)-1][1],src_data[len(src_data)-1][1]-src_data[0][0],1],synth_data[0]]]
		else:
			sil_idx_list = self.sort_dur(src_sil_dur_list,True)
			sil_idx = sil_idx_list[top]
			cut_idx = src_sil_idx_list[sil_idx]
			if (cut_idx == len(src_data)-1):
				return self.align_srt(src_data[:cut_idx],synth_data,0,filename) + [[[-1],src_data[cut_idx],[]]]
			elif (cut_idx==0):
				return [[[-1],src_data[cut_idx],[]]] + self.align_srt(src_data[cut_idx+1:],synth_data,0,filename)
			else:
				cut_percent = src_speech_dur_percent[src_speech_idx_list.index(cut_idx-1)]
				file_pointer = open("source splits_details.txt", "a+")
				fcntl.flock(file_pointer, fcntl.LOCK_EX)
				file_pointer.write(filename+" : src split at "+str(src_data[cut_idx][0])+"\n") #providng stats
				fcntl.flock(file_pointer, fcntl.LOCK_UN)
				file_pointer.close()
				#print("DEBUG:")
				#print(src_sil_dur_list)
				#print(src_speech_dur_percent)
				#print(tts_speech_dur_percent)
				tts_near = min(enumerate(tts_speech_dur_percent), key=lambda x: abs(x[1]-cut_percent))
				label_idx = tts_near[0]
				star_count_front=5
				star_count_back=5.0

				label_to_be_cut = tts_speech_label_list[label_idx]
				
				output,_ = self.isFuncWordThere(label_to_be_cut, function_words,1)
				#print(output)
				if(output):
					print("Found a break, by default")
					#print(label_to_be_cut)
					file_pointer = open("splits_details.txt", "a+")
					fcntl.flock(file_pointer, fcntl.LOCK_EX)
					file_pointer.write(filename+" : "+label_to_be_cut + "\n") #providng stats
					fcntl.flock(file_pointer, fcntl.LOCK_UN)
					file_pointer.close()
				else:
					break_found=False
					if(label_idx < len(tts_speech_label_list)-1):
						hypothesized_label_idx = label_idx+1
						while(star_count_front >= 0 and hypothesized_label_idx < len(tts_speech_label_list)-1 and abs(cut_percent-tts_speech_dur_percent[hypothesized_label_idx]) < threshold_difference):
							output,star_count_front = self.isFuncWordThere(tts_speech_label_list[hypothesized_label_idx],function_words,star_count_front)
							if(output):
								print("Corrected to break : " + tts_speech_label_list[hypothesized_label_idx])
								tts_near = list(tts_near)
								tts_near[1] = tts_speech_dur_percent[hypothesized_label_idx]
								tts_near[0] = hypothesized_label_idx
								break_found=True
								file_pointer = open("Corrected_by_breaks.txt", "a+")
								fcntl.flock(file_pointer, fcntl.LOCK_EX)
								file_pointer.write(filename+" : "+tts_speech_label_list[hypothesized_label_idx] + "\n") #providng stats
								fcntl.flock(file_pointer, fcntl.LOCK_UN)
								break
							hypothesized_label_idx+=1
					elif(label_idx > 1 and not break_found):
						hypothesized_label_idx = label_idx-1
						while(star_count_back >= 0 and hypothesized_label_idx > 0 and abs(cut_percent-tts_speech_dur_percent[hypothesized_label_idx]) < threshold_difference):
							output,star_count_back = self.isFuncWordThere(tts_speech_label_list[hypothesized_label_idx],function_words,star_count_back)
							if(output):
								print("Corrected to break : " + tts_speech_label_list[hypothesized_label_idx])
								tts_near = list(tts_near)
								tts_near[1] = tts_speech_dur_percent[hypothesized_label_idx]
								tts_near[0] = hypothesized_label_idx
								file_pointer = open("Corrected_by_breaks.txt", "a+")
								fcntl.flock(file_pointer, fcntl.LOCK_EX)
								file_pointer.write(filename+" : "+tts_speech_label_list[hypothesized_label_idx] + "\n") #providng stats
								fcntl.flock(file_pointer, fcntl.LOCK_UN)
								break
							hypothesized_label_idx-=1
					file_pointer = open("splits_details.txt","a+")
					fcntl.flock(file_pointer, fcntl.LOCK_EX)
					file_pointer.write(filename+" : "+tts_speech_label_list[tts_near[0]]+"\n") #providng stats
					fcntl.flock(file_pointer, fcntl.LOCK_UN)
					file_pointer.close()
				if abs(cut_percent-tts_near[1]) > threshold_difference:
					#print("merged",cut_percent,tts_near[1])
					if top+1<len(sil_idx_list):
						return self.align_srt(src_data,synth_data,top+1,filename)
					else:
						return [[[1],[src_data[0][0],src_data[len(src_data)-1][1],src_data[len(src_data)-1][1]-src_data[0][0],1],[synth_data[0][0],synth_data[len(synth_data)-1][1],synth_data[len(synth_data)-1][1]-synth_data[0][0],1]]]
				else:
					tts_idx = tts_near[0]+1
					return self.align_srt(src_data[:cut_idx],synth_data[:tts_idx],0,filename) + [[[-1],src_data[cut_idx],[]]] + self.align_srt(src_data[cut_idx+1:],synth_data[tts_idx:],0,filename)

	def merge_src_sil(self,data_temp1):#this section of code removes very small silences
		max_dur=0.5 # seconds
		sil_dur_list=[]
		sil_dur_idx=[]
		for i in range(len(data_temp1)):
			if (data_temp1[i][3]==-1):
				sil_dur_list.append(data_temp1[i][1]-data_temp1[i][0]) #appends the duration of same
				sil_dur_idx.append(i) #appends only the index of i which is sil

		if (len(sil_dur_list)<=0):
			return data_temp1

		sil_idx_list = self.sort_dur(sil_dur_list,False) #sort based on largest sil
		sil_idx = sil_idx_list[0] #largest_sil
		dur = sil_dur_list[sil_idx] #duration of the largest sil
		dur_idx = sil_dur_idx[sil_idx] #where exactly is the largest sil lying in data
		while(True):
			if (dur < max_dur):
				if (dur_idx == 0 and dur < 0.05): #if there is a silence at begining
					data_temp1[dur_idx+1][0] = data_temp1[dur_idx][0] 
					data_temp1[dur_idx+1][2] = data_temp1[dur_idx+1][1] - data_temp1[dur_idx+1][0]
					del(data_temp1[dur_idx])
				elif (dur_idx == len(data_temp1)-1 and dur < 0.05): #if there is silence at the end
					data_temp1[dur_idx-1][1] = data_temp1[dur_idx][1]
					data_temp1[dur_idx-1][2] = data_temp1[dur_idx-1][1] - data_temp1[dur_idx-1][0]
					del(data_temp1[dur_idx])
				elif(dur_idx !=0  and dur_idx != len(data_temp1)-1):
					data_temp1[dur_idx-1][1] = data_temp1[dur_idx+1][1]
					data_temp1[dur_idx-1][2] = data_temp1[dur_idx-1][1] - data_temp1[dur_idx-1][0]
					del(data_temp1[dur_idx:dur_idx+2]) #merging the two non silence parts and ignoring the silence in middle
				sil_dur_list=[]
				sil_dur_idx=[]
				for i in range(len(data_temp1)):
					if (data_temp1[i][3]==-1):
						if(i != 0 and i!= len(data_temp1)-1):
							sil_dur_list.append(data_temp1[i][1]-data_temp1[i][0])
							sil_dur_idx.append(i)
						elif(data_temp1[i][1]-data_temp1[i][0]<0.05):
							sil_dur_list.append(data_temp1[i][1]-data_temp1[i][0])
							sil_dur_idx.append(i)

				if (len(sil_dur_list)<=0):
					return data_temp1

				sil_idx_list = self.sort_dur(sil_dur_list,False)
				sil_idx = sil_idx_list[0]
				dur = sil_dur_list[sil_idx]
				dur_idx = sil_dur_idx[sil_idx]
			else:
				break
		return data_temp1
	def main(self):
		src_align_file_name=self.src_align_file_name
		tts_align_file_name=self.tts_align_file_name
		source_nosound_video_file=self.source_nosound_video_file
		tts_audio_file=self.tts_audio_file
		filename=self.filename
		final_videos_dir=self.final_videos_dir


		src_align_list=self.read_boundary_file(src_align_file_name)
		#print("Intital src_align_list: ", src_align_list)
		#src_align_time_list=self.read_boundary_file(src_align_file_name)
		src_align_time_list=self.merge_src_sil(src_align_list)
		#print("After merge_sil_src: ", src_align_time_list)
		tts_align_list=self.read_ctm_boundary_file(tts_align_file_name)
		tts_sil_merge_data=self.merge_sil(tts_align_list)
		tts_align_time_list = tts_sil_merge_data['time_align_list']
		#print("MANO: ",src_align_time_list)
		#print("MANO: ",tts_align_time_list)
		# Recurssive code - checks the longest silence down to the shortest
		alignment = self.align_srt(src_align_time_list,tts_align_time_list,0,filename)
		print(alignment)

		# Load video and audio files
		video_nosound = source_nosound_video_file
		#video_nosound = VideoFileClip(source_nosound_video_file)
		audio = AudioFileClip(tts_audio_file)

		video_list=[]
		
		for i in range(len(alignment)):
			if(alignment[i][0][0] == -1):
				temp_clip = video_nosound.subclip(alignment[i][1][0], alignment[i][1][1])
				video_final = concatenate_videoclips([temp_clip])
				#video_final.write_videofile(outFileName)
				video_list.append(temp_clip)
			elif(alignment[i][0][0] == 1):
				video_clip = video_nosound.subclip(alignment[i][1][0], alignment[i][1][1])
				audio_clip = audio.subclip(alignment[i][2][0], alignment[i][2][1])
				print(alignment[i][2][1]-alignment[i][2][0])
				#new_audio = (audio_clip.fx( vfx.audio_fadein, 1).fx( vfx.audio_fadeout, 1))
				#new_audio = afx.audio_fadein(audio_clip, duration=0.02)
				new_audio = audio_clip			
				length_audio_clip = new_audio.duration
				print(length_audio_clip)
				video_warp_ratio = (alignment[i][1][1]-alignment[i][1][0])/length_audio_clip
				warp_video = video_clip.fx(vfx.speedx, video_warp_ratio)
				warp_video.audio = new_audio
				video_final = concatenate_videoclips([warp_video])
				#video_final.write_videofile(outFileName)
				video_list.append(warp_video)
			else:
				print('Error!!')
				sys.exit()
		video_final = concatenate_videoclips(video_list)
		#video_final.write_videofile(final_videos_dir+'final_videos/'+filename+'.mp4')
		try:
				video_final.write_videofile(final_videos_dir+'final_videos/'+filename+'.mp4')
		except IndexError:
				video_final = video_final.subclip(t_end=(video_final.duration - 1.0/video_final.fps))
				video_final.write_videofile(final_videos_dir+'final_videos/'+filename+'.mp4')
		return

			
	# Main function
	if __name__ == "__main__":
	    main()

