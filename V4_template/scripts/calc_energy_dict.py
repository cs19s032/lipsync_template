import wave
import numpy as np
import sys
import librosa 

class calc_energy(object):
	def __init__(self,wave_file,ste_file):
		self.wav_file=wave_file
		self.ste_file=ste_file

	def main(self):
		wav_file = self.wav_file
		ste_file = self.ste_file
		
		hop_length = 256
		frame_length = 512
		
		x, sr = librosa.load(wav_file)
		#sr will by default 22050 by librosa, change it to 48000 incase if wav file is 48KHz
		
		energy = np.array([sum(abs(x[i:i+frame_length]**2)) for i in range(0, len(x), hop_length) ])
		amax = max(energy)
		amin = min(energy)
		norm = [(float(i)-amin)/(amax-amin) for i in energy]

		with open(ste_file, 'w') as f:
			for item in norm:
				f.write("%f\n" % item)
	
	if __name__ == "__main__":
		main()

