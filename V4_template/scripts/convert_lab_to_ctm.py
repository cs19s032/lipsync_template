#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv
results=[]
text_file= sys.argv[2]
list_of_lab_files = sys.argv[1]
sylmap = sys.argv[4]
target_dir = sys.argv[3]
Path(target_dir).mkdir(parents=True, exist_ok=True)
dict = {}
with open(sylmap) as sylmap_f:
    syline = sylmap_f.readline()
    while syline:
        syline = syline.split()
        dict[syline[0]]=syline[1]
        syline = sylmap_f.readline()
lab_fp = open(list_of_lab_files, "r")
lab_files = lab_fp.readlines()
current_lab = []
for line in lab_files:
        current_lab_line = line[:-1]
        current_lab.append(current_lab_line)
only_text = open(text_file,"r")
only_text_lines = only_text.readlines()
current_text = []
for line in only_text_lines:
        current_text_line = line[:-1]
        current_text.append(current_text_line)
if(len(current_lab)!=len(current_text)):
    print("ERROR: The length of lists no matching")
    print(str(len(current_lab)) +"!="+str(len(current_text)))
    exit()
cnt = 0
wrdcnt = 0
for i in range(len(current_lab)):
 with open(current_lab[i],"r") as cl:
     line= cl.readline() #skipping the hash at top
     line = cl.readline()
     words = current_text[cnt].split()
     print(words)
     currentword=""
     wrdcnt = 0
     while line:
     	line_columns= line.split()
     	line_columns[2] = dict[line_columns[2]]
     	#print(line_columns[2])
     	if(line_columns[2] != "SIL"):
             if(currentword==""):
                  currentword = line_columns[2]
                  print(currentword + " x " + words[wrdcnt] )
             if(currentword != line_columns[2]):
                   currentword =currentword+line_columns[2]
                   print(currentword)
             if(currentword == words[wrdcnt]):
     	      	    print(currentword + "Entered")
     	      	    currentword = ""
     	      	    wrdcnt = wrdcnt+1
     	line = cl.readline()
     cnt= cnt+1
