#!/bin/sh

#  splitAlignments.py
#  
#
#  Created by Mano Ranjith Kumar on 9/3/21.
#
#
#
from pathlib import Path
import sys,csv

def closest(lst, K):
      
    return lst[min(range(len(lst)), key = lambda i: abs(lst[i]-K))]

missing = 0
ctm_file = sys.argv[1]
lab_file = sys.argv[2]
mylabfile = Path(lab_file)
target_dir = sys.argv[3]
Path(target_dir).mkdir(parents=True, exist_ok=True)
if mylabfile.is_file():
   print("Fonund lab file correcting boundaries")
else:
   print("Lab file named "+ lab_file +" is missing")
lab_values =[]
with open(lab_file,"r") as lp:
    line = lp.readline() # Skipping hash
    line = lp.readline()
    while line:
       line = line.strip()
       columns = line.split()
       lab_values.append(float(columns[0]))
       line = lp.readline()
       
with open(ctm_file,"r") as fp:
   line = fp.readline()
   while line:
       line = line.strip()
       columns = line.split()
       closest_start_value = closest(lab_values, float(columns[2]))
       end_value = float(columns[3]) + float(columns[2])
       closest_end_value = closest(lab_values,end_value)
       if(closest_start_value != closest_end_value):
        if(closest_start_value - float(columns[2]) <= 0.300):
           columns[2] = closest_start_value
        if(closest_end_value - end_value <= 0.300):
           columns[3] = closest_end_value - float(columns[2])
       with open(target_dir+'/'+columns[0]+'.gtxt', 'a+') as the_file:
         line = " ".join([str(i) for i in columns])
         the_file.write(line+"\n")
       line = fp.readline()
