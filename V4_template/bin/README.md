## Run the script (LongSilence) to find the silence and voiced regions in speech

```bash
LongSilence ctrlFile waveFile (input -- parameters in control file)  sigFile (output time (s), float)  boundaryFile (+1 ==> voiced, -1 ==> unvoiced thresSilence ( minimum in ms to be considered unvoiced) thresVoiced (minimum in ms to be considered voiced)
```

#### Control file: ```fe-words.base```

Set global variables

```bash
windowSize int 1102 (Window Size)
waveType int 6 (.wav file)
winScaleFactor float 5
gamma float 0.0001
fftOrder int 0
fftSize int 0
frameAdvanceSamples int 110 (Window Shift)
medianOrder int 10
thresEnergy float 0.4
thresZero float 0.0
thresSpecFlatness float 0.0
samplingRate int 44100 (Sampling Rate)
percentFrames int 1
minFrequency float 0
maxFrequency float 22050 (Not more than half of Sampling Rate)
filterOrder int 0
lpOrder int 10
vad int 1
```

Examples
```bash
bin/LongSilence bin/fe-words.base samples/Deepa_lec015-sub5-mono.wav samples/sigFile.txt samples/boundaryFile.txt 100 50
```
