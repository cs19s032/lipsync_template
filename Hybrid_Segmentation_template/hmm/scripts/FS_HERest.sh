#!/bin/tcsh

set count = $1
set countplusone = $2
set NoI = $3
set NJ = $4
set outDir = $5

mkdir -p hmm_GMV/hmm0
cp hmm_GMV/macros hmm_GMV/hmm0/
cp hmm_GMV/hmmdefs hmm_GMV/hmm0/
while ($count < $NoI)
	mkdir -p hmm_GMV/hmm$countplusone
	echo "Embedded Reestimation ---- Iteration $countplusone"
	# HERest -S trnlist1 -I lab1 -H dir1/hmmdefs1 -M dir2 -p 1 hmmlist    # part1
	# HERest -S trnlist2 -I lab2 -H dir1/hmmdefs2 -M dir2 -p 2 hmmlist    # part2
	# HERest                      -H dir1/hmmdefs  -M dir2 -p 0 hmmlist  dir2/*.acc
	less mlflstmap | parallel -j$NJ -v  --colsep ' ' "HERest -T 1 -C config_files/config_feature -I {1} -t 250.0 150.0 6000.0 -S {2} -H hmm_GMV/hmm$count/macros -H hmm_GMV/hmm$count/hmmdefs -M hmm_GMV/hmm$countplusone -p {3} phonelist_without_context" 
	HERest -T 1 -C config_files/config_feature -t 250.0 150.0 6000.0  -H hmm_GMV/hmm$count/macros -H hmm_GMV/hmm$count/hmmdefs -M hmm_GMV/hmm$countplusone  -p 0 phonelist_without_context hmm_GMV/hmm$countplusone/*.acc
	#HERest -T 1 -C config_files/config_feature -I phones.mlf -t 250.0 150.0 6000.0 -S flist -H hmm_GMV/hmm$count/macros -H hmm_GMV/hmm$count/hmmdefs -M hmm_GMV/hmm$countplusone phonelist_without_context 
	@ count++
	@ countplusone++
end
