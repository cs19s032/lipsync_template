#!/bin/perl
#Argument -> lab list
open(F, "<$ARGV[0]");
while(<F>)
{
chomp;
($f) = split(" ");
#print "File : $f\n";
$a = substr($f, 0, length($f) - 4);
open(FW, ">>Transcription_splice.txt");
$count = 1;
$prevTime = 0;
open(FR, "<syllab/$f");
while(<FR>)
{
chomp;
($c1, $c2, $c3) = split(" ");
if($count < 10 ) {
$syllableFileName = $a."-00".$count;
}
elsif($count <100) {
$syllableFileName = $a."-0".$count;
}
else {
$syllableFileName = $a."-".$count;
}
if($c1 eq "#")
{
}
else
{
$sample1=$prevTime*48000;
$sample2=$c1*48000;
$prevTime=$c1;
$wavdir=$ENV{WAV_48};
`ch_wave $wavdir/$a.wav -o splice_wav/$syllableFileName.wav -from $sample1 -to $sample2\n`;
$count = $count + 1;
print FW "splice_wav/$syllableFileName $c3\n";
}
}
}
