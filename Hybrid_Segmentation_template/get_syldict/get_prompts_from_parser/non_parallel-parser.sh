#!/bin/bash

unique_words=$1
phone_file_name=$2
nj=$3
rm -r ./syls/*
rm ${phone_file_name}.cls ${phone_file_name}

cat $unique_words | parallel -k -j $nj "valgrind ./phonify_text/unified-parser {1} ./syls/{1}.txt 1 0 0 0"

while IFS= read -r word; do
	if [ $word != "SIL" ]
	then
		#valgrind phonify_text/unified-parser $word ./syls/$word.txt 1 0 1 0
		output=`cat ./syls/$word.txt`
		echo $word $output >>${phone_file_name}.cls
		#rm ${word}.wordpronunciation
	fi
done < $unique_words
echo "SIL SIL" >> ${phone_file_name}.cls
cp ${phone_file_name}.cls ${phone_file_name}
#bash get_phone_mapped_text.sh ${phone_file_name}
