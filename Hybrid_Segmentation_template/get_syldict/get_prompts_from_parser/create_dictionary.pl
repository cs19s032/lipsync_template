#!/usr/local/bin/perl 

open(F1, "<unique_syllables");
#binmode (F1);
open(W, ">syldict");
while(<F1>) {
	chomp;
	my $word = $_;
	for ($word) {
        	s/^\s+//;
        	s/\s+$//;
	}
	print W "$word ";
	system("./bin/unifiedparser $word 1 0 1 0");
	open(F2, "<wordpronunciation");
	my $count;
	while(<F2>) {
		$count = ($_ =~ s/0/0/g);
	}
	close(F2);
	if($count gt 1){
		system("./bin/unifiedparser $word 1 0 0 0");
	}
	open(F2, "<wordpronunciation");
	while(<F2>) {
		chomp;
		print W "$_\n";
	}
	close(F2);
}
close(W);
close(F1);
