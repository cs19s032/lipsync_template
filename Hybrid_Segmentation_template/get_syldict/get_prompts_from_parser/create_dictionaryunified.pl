#!/usr/local/bin/perl 

open(F1, "<etc/txt.done.data");
#binmode (F1);

%dict={};
%hoh={};
$prevword='';
$countrow=1;$countword=1;
while(<F1>) {
	chomp;
	@text =  split(/\"/, $_);
	$text[1] =~ s/[#%&\$*+()!?\.\,\']//g;
	$text[1] =~ s/-/ /g;
	@words = split(/ /, $text[1]);
	@monoWords;
	@syllWords;
	$countword=0;
	foreach $word(@words)
	{
		#print "\nrow $countrow word $countword";
		$word =trim($word);
		if($word ne NULL && $word ne "*" && $word ne ""){
			$dict{$word}=$dict{$word}+1;
			system("rm wordpronunciationsyldict");
			system("valgrind unified-parser   $word    1 0 1 0");
			if(open(F2, "<wordpronunciationsyldict")){
				while(<F2>) {
					chomp;
					$_ =~ s/\(set\! wordstruct \'\(//g;
					$_ =~ s/\)//g;
					$_ =~ s/[0 ]//g;
					@monoWords =  split(/\(\(/, $_);
				}
				close(F2);
			}else{
				print "errormono $word\n";
			}
			system("valgrind unified-parser   $word   1 1 1 0");
			if(open(F2, "<wordpronunciationsyldict")){
				my $count;
				while(<F2>) {
					chomp;
					$_ =~ s/\(set\! wordstruct \'\(//g;
					$_ =~ s/\)//g;
					$_ =~ s/[0 ]//g;
					#print "$_ \n";
					@syllWords =  split(/\(\(/, $_);
				}
				close(F2);
			}else{
				print "errorsyl $word\n";
			}
			for my $i (0 .. $#syllWords)
			{
				if($syllWords[$i] ne NULL &&  $syllWords[$i] ne ""){
					#print "$syllWords[$i] $monoWords[$i]\n";
					$syllWords[$i] =~ s/"//g;
					$syllWords[$i] =~ s/_beg//g;
					$syllWords[$i] =~ s/_mid//g;
					$syllWords[$i] =~ s/_end//g;
					$monoWords[$i] =~ s/""/ /g;
					$monoWords[$i] =~ s/"//g;
					if($monoWords[$i] ne NULL &&  $monoWords[$i] ne ""){
						$hoh{$syllWords[$i]}{"beg-$monoWords[$i]_end"}+=1;		
					}
				}
			}
			close(F2);
		}
		$prevword =$word;
		$countword++;
	}
	$countrow++;
}
close(F1);
system("rm wordpronunciationsyldict");
open(W, ">syldict");
print W "SIL SIL\n";
print W "sp sp\n";
foreach $item (sort keys %hoh){
  foreach $iteminitem (sort keys %{$hoh{$item}}){
	if (!($item =~ /^[a-zA-Z]+$/)){
		if (!($item =~ /^#+$/)){
			print W "$item $iteminitem\n";
		}
	}
	#print W "$item\t$iteminitem\t$hoh{$item}{$iteminitem}\n";
  }
}

close(W);



sub trim($)
{
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
