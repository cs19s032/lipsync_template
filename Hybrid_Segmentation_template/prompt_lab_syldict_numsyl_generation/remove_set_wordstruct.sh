#!/bin/bash
##############################################################################
#										#	
#	created by Mano Ranjith Kumar M					#
#										#
#	date : Jul 11 2022							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#
#										#	
##############################################################################


wordfile=$1
sed -i "s/(\s*\t*set\!\s*\t*wordstruct\s*\t*'\s*\t*(\s*\t*(\s*\t*(\s*\t*//g" ${wordfile}
sed -i "s/\s*\t*)\s*\t*0\s*\t*)\s*\t*)\s*\t*)\s*\t*//g" ${wordfile}
sed -i "s/[[:space:]]\+/ /g" ${wordfile}
sed -i "s/\"//g" ${wordfile}
sed -i "s/0//g" ${wordfile}	
sed -i "s/)//g" ${wordfile}	
sed -i "s/(//g" ${wordfile}
sed -i "s/  / /g" ${wordfile}
sed -i "s/  / /g" ${wordfile}
