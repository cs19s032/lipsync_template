expected_args=4
if [ $# -ne $expected_args ];then
    echo "Parameters Missing...!"
    echo "Parameter 1: flist"                                           
    echo "Parameter 2: mlf_file"
    echo "Parameter 3: nSplit"  
    echo "Outdir"       
    exit 1                                  
fi

flist=$1
mlfFile=$2
nSplit=$3
outDir=$4

rm tempp tempq
mkdir -p $outDir

count=`less $flist |wc -l`
let count1=$count/$nSplit
let nc=$nSplit-1
let val1=$count1*$nc

cp $mlfFile phones1.mlf
sed -i 's/\.lab/_lab/' phones1.mlf
sed -i 1d phones1.mlf

#cat $flist | sort > list1
#less phones1.mlf | tr '\n' ' ' |tr '.' '\n' | sort >list2

cat $flist  > list1
less phones1.mlf | tr '\n' ' ' |tr '.' '\n'  >list2
sed -i 's/^ //' list2
sed -i 's/ $//' list2

seq $count1 $count1 $val1 |parallel -k  "echo \"less list1 | head -{1} |tail -$count1 >$outDir/\" >> tempp"
seq $count1 $count1 $val1 |parallel -k  "echo \"less list2 | head -{1} |tail -$count1 >$outDir/\" >> tempq"

let res=$count-$val1
echo $count $count1 $res
echo "less list1 |tail -$res >$outDir/" >>tempp
echo "less list2 |tail -$res >$outDir/" >>tempq

count2=`less tempp |wc -l`
seq 1 $count2 > c1

paste -d'\0' tempp c1 > t1
sed -i 's/$/.lst/' t1
paste -d'\0' tempq c1 > t2
sed -i 's/$/.mlf/' t2

bash t1
bash t2

ls $outDir/*.mlf |parallel -k "sed -i 's/$/ ./' {1}"
ls $outDir/*.mlf |parallel -k "sed -i 's/\s\+/\n/g' {1}"
ls $outDir/*.mlf |parallel -k "sed -i 's/_lab/\.lab/' {1}"
ls $outDir/*.mlf |parallel -k "sed -i '1i #!MLF!#' {1} "
