#!/bin/sh

export DSPLIB=$PWD/front-end-dsp/src
export NISTINC=$PWD/front-end-dsp/nist/include
export NISTLIB=$PWD/front-end-dsp/nist/lib
export WAV_48=$1/wav
export MFC_48=$1/features/mfc
export NJ=$2
cd front-end-dsp/Segmentation ; make -B ; cd ../../
sh src/clean.sh
make -B
sh src/create_directories.sh

echo "HS start" >>timelog.txt
date >> timelog.txt
cd hmm/
cat syldict | cut -d " " -f2- > temp
cp syldict syldict_without_context
sed -e 's/beg\-//g' -i syldict_without_context
sed -e 's/\_end//g' -i syldict_without_context
sed -e 's/\s\+/\n/g' -i temp
sort -u temp > phonelist
cat syldict_without_context | cut -d " " -f2- > temp
sed -e 's/\s\+/\n/g' -i temp
sort -u temp > phonelist_without_context
cd ../
cp hmm/syldict_without_context dict_with_end
sed -e 's/$/ end/' -i dict_with_end
cp Phonelist_Description/Vowels hmm/vowels

#echo "Downsampling Wavefiles to 16KHz"
#ls $WAV_48/ > wav_list
#tcsh hmm/scripts/sr_change_48_16.sh wav_list
#cp wav_list hmm/
cd hmm/
#echo "Downsampling Wavefiles to 8KHz for splicing"
#tcsh scripts/sr_change.sh wav_list
#s wav_8KHz/*.wav > wav_list
ls prompt-lab > list
ls prompt-lab/ >wav_list
sed -i 's#.lab#.wav#g' wav_list
sed -i 's#^#'"$WAV_48/"'#g' wav_list 
sh scripts/prepare_list.sh
cp list ../

tcsh scripts/dict.sh phonelist_without_context || exit -1
tcsh scripts/dict_with_context.sh phonelist
#tcsh scripts/map_table.sh wav_list
#echo "Extracting MFCC's from 8KHz wavefiles"
#------- extract 8KHz MFCCs and keep it in Database
#sh scripts/extract_feature.sh

sort phonelist_without_context > phonelist_sorted
sort vowels > vowels_sorted
comm -23 phonelist_sorted vowels_sorted > consonants
sed -i '/sp/d' consonants

cd prompt-lab/
sed -i 's/_v//' *
sed -i 's/_iv//' *
sed -i 's/_uv//' *
sed -i 's/_GEM//' *
sed -i 's/_BEG//' *
sed -i 's/_MID//' *
sed -i 's/_END//' *
sed -i 's/_beg//' *
sed -i 's/_mid//' *
sed -i 's/_end//' *
sed -i 's/_bg//' *
sed -i 's/_eg//' *
sed -i 's/-x//' *
cd ../

perl scripts/create_transcription.pl
./createMLF

HLEd -l '*' -d syldict_without_context -i phones.mlf mkphones0.led words.mlf
HLEd -l '*' -d syldict -i phones_with_context.mlf mkphones0.led words.mlf
sed -i 's/wav_8KHz/*/g' words.mlf
#cat map_table | cut -d " " -f2 > flist

ls prompt-lab/ >flist
sed -i 's#.lab#.mfc#g' flist
sed -i 's#^#'"$MFC_48/"'#g' flist 
#sed -i 's/\.wav/\.mfc/g' flist

#sed -i 's/\.wav/\.mfc/g' flist

HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_3s_2m
HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_5s_2m
HCompV -C config_files/config_feature -f 0.01 -m -S flist -M hmm_GMV protos/proto_1s_2m
cat HMM_macro_header hmm_GMV/vFloors > hmm_GMV/macros
tcsh scripts/hdef.sh vowels consonants phonelist 
tcsh scripts/FS_HERest.sh 0 1 14

echo "Sentence Lvl Alignment with SIL"
HVite -l output_lab_with_SIL/ -C config_files/config -a -H hmm_GMV/hmm14/macros -H hmm_GMV/hmm14/hmmdefs -y lab -o SM -I words.mlf -S flist syldict_without_context phonelist_without_context
perl scripts/normallab_with_SIL.pl
tcsh scripts/remsil_initial.sh list
rm Transcription.txt
perl scripts/create_transcription_SIL_removed.pl
mv words.mlf words_with_SIL.mlf
./createMLF
sed -i 's/wav_8KHz/*/g' words.mlf

echo "Sentence Lvl Alignment"
HVite -l output_lab/ -C config_files/config -a -H hmm_GMV/hmm14/macros -H hmm_GMV/hmm14/hmmdefs -y lab -o SM -I words.mlf -S flist syldict_without_context phonelist_without_context
perl scripts/normallab.pl

cd ../
#tcsh src/gd_correction.sh l	ist hmm_syllable_lab_with_begin_end_phone intermediate_output_lab
rm -rf part* sublists
split -da 4 -l $((`wc -l < list`/$NJ)) list part --additional-suffix=".sublist"
partcount=`ls part*|wc -l`;
echo "count part : $partcount"
ls part* > sublists
echo "Adding Begin & End Phone to Syllable Lab Files.... I"
rm -f hmm_syllable_lab_with_begin_end_phone/*
less sublists | parallel -v -j$NJ "tcsh src/add_phone2syl.sh {}"
rm -f intermediate_output_lab/*
echo "Syllable Boundary Correction.... I"
less sublists | parallel -v -j$NJ "tcsh src/gd_correction.sh {} hmm_syllable_lab_with_begin_end_phone intermediate_output_lab"
cp sublists part* hmm/
cp intermediate_output_lab/* hmm/syllab_with_sil/
cd hmm/
echo "Remove very small silences.... I"
rm -f syllab/*
less sublists | parallel -v -j$NJ "tcsh scripts/remsil.sh {}"

echo "Splicing Waveforms & Extracting Features.... I"
rm -f Transcription_splice.txt
less sublists | parallel -v -j$NJ "perl scripts/splice.pl {}"
rm sublists part*
./createMLF_splice
ls splice_wav/ > wav_list_splice
sed -i 's/^/splice_wav\//' wav_list_splice 
rm map_table
tcsh scripts/map_table.sh wav_list_splice
cat map_table | cut -d " " -f2 > flist_splice
python remove_splice_units.py list > log_bad.sh
sh log_bad.sh
./createMLF_splice
rm -rf part* sublists
split -da 4 -l $((`wc -l < map_table`/$NJ)) map_table part --additional-suffix=".sublist"
ls part* > sublists
less sublists | parallel -v -j$NJ "sh scripts/extract_feature_splice.sh {}"
rm sublists part*

HLEd -l '*' -d syldict -i phones_splice.mlf mkphones0.led words_splice.mlf
cp hmm_GMV/hmmdefs hmm0/hmmdefs
cp hmm_GMV/macros hmm0/macros
echo "HERest I start" >>../timelog.txt
date >> ../timelog.txt


echo "Embedded Reestimation.... I"
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm0/macros -H hmm0/hmmdefs -M hmm1 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm1/macros -H hmm1/hmmdefs -M hmm2 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm2/macros -H hmm2/hmmdefs -M hmm3 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm3/macros -H hmm3/hmmdefs -M hmm4 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm4/macros -H hmm4/hmmdefs -M hmm5 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm5/macros -H hmm5/hmmdefs -M hmm6 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm6/macros -H hmm6/hmmdefs -M hmm7 phonelist
HVite -l output_lab_splice/ -C config_files/config_splice -a -H hmm7/macros -H hmm7/hmmdefs -y lab -o SM -I words.mlf -S flist syldict phonelist 

echo "HERest I end" >>../timelog.txt
date >> ../timelog.txt

perl scripts/normallab_splice.pl

cd ../
echo "Adding Begin & End Phone to Syllable Lab Files.... II"
rm -f hmm_syllable_er_syl_lab_with_begin_end_phone/*
less sublists | parallel -v -j$NJ "tcsh src/add_phone2syl_2.sh {}"
rm -f output_lab_syllable/*
echo "Syllable Boundary Correction.... II"
less sublists | parallel -v -j$NJ "tcsh src/gd_correction_2.sh {} hmm_syllable_er_syl_lab_with_begin_end_phone output_lab_syllable"
cp output_lab_syllable/* hmm/syllab_with_sil_2/
rm results/*
cp sublists part* hmm/
cd hmm/
echo "Remove very small silences.... II"
rm -f syllab_2/*
less sublists | parallel -v -j$NJ "tcsh scripts/remsil_2.sh {}"

echo "Splice I start" >>../timelog.txt
date >> ../timelog.txt

echo "Splicing Waveforms & Extracting Features.... II"
rm -rf splice_wav/
rm -f Transcription_splice_2.txt
mkdir splice_wav
less sublists | parallel -v -j$NJ "perl scripts/splice_2.pl {}"
./createMLF_splice_2


echo "Splice I start" >>../timelog.txt
date >> ../timelog.txt


ls splice_wav/ > wav_list_splice
sed -i 's/^/splice_wav\//' wav_list_splice 
rm map_table
tcsh scripts/map_table.sh wav_list_splice
cat map_table | cut -d " " -f2 > flist_splice
python remove_splice_units_2.py list > log_bad.sh
sh log_bad.sh ; rm log_bad.sh
./createMLF_splice_2
rm -rf part* sublists
split -da 4 -l $((`wc -l < map_table`/$NJ)) map_table part --additional-suffix=".sublist"
ls part* > sublists
less sublists | parallel "sh scripts/extract_feature_splice.sh {}"
rm sublists part*

echo "Splice I end" >>../timelog.txt
date >> ../timelog.txt

HLEd -l '*' -d syldict -i phones_splice.mlf mkphones0.led words_splice.mlf
cp hmm7/hmmdefs hmm0/hmmdefs
cp hmm7/macros hmm0/macros


echo "HS II start" >>../timelog.txt
date >> ../timelog.txt

echo "Embedded Reestimation.... II"
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm0/macros -H hmm0/hmmdefs -M hmm1 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm1/macros -H hmm1/hmmdefs -M hmm2 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm2/macros -H hmm2/hmmdefs -M hmm3 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm3/macros -H hmm3/hmmdefs -M hmm4 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm4/macros -H hmm4/hmmdefs -M hmm5 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm5/macros -H hmm5/hmmdefs -M hmm6 phonelist
HERest -C config_files/config_feature_2 -I phones_splice.mlf -t 250.0 150.0 6000.0 -S flist_splice -H hmm6/macros -H hmm6/hmmdefs -M hmm7 phonelist > log_remove

echo "HS II end" >>../timelog.txt
date >> ../timelog.txt


cat log_remove | cut -d " " -f7 | cut -d "/" -f2 | cut -d "-" -f1 | sort | uniq > list_remove
HVite -l '*' -C config_files/config_splice -H hmm7/macros -H hmm7/hmmdefs -i align.mlf -m -t 250.0 150.0 6000.0 -y lab -I words_splice.mlf -S flist_splice syldict phonelist
perl scripts/combine_syllable_level_alignment.pl
cat list_remove_duplicates | uniq > list_remove
tcsh scripts/lab_remove.sh list_remove

HVite -l fal_hybrid_sentence/ -C config_files/config -a -H hmm7/macros -H hmm7/hmmdefs -y lab -I phones_with_context.mlf -S flist dict_with_context phonelist
perl scripts/normallab_2.pl
tcsh scripts/lab_copy.sh list_remove
cp hybrid_phone_lab/* ../output_lab_phone/
sed -e 's/beg\-//g' -i ../output_lab_phone/*
sed -e 's/\_end//g' -i ../output_lab_phone/*

HVite -l output_lab_hybrid/ -C config_files/config -a -H hmm7/macros -H hmm7/hmmdefs -y lab -o MN -I phones_with_context.mlf -S flist dict_with_context phonelist
HVite -l output_lab_hmm/ -C config_files/config -a -H hmm_GMV/hmm14/macros -H hmm_GMV/hmm14/hmmdefs -y lab -o MN -I phones.mlf -S flist dict phonelist_without_context
sed -e 's/beg\-//g' -i output_lab_hybrid/*
sed -e 's/\_end//g' -i output_lab_hybrid/*

perl scripts/cal_likelihood_category.pl
perl scripts/cal_likelihood_category_hybrid.pl
cd ../
perl hmm/scripts/normallab_hmm.pl

sed -e 's/sp/SIL/g' -i output_lab_syllable/*
sed -e 's/sp/SIL/g' -i output_lab_phone/*


echo "HS end" >>timelog.txt
date >> timelog.txt
echo "Completed"
