export DSPLIB=$PWD/front-end-dsp/src
export NISTINC=$PWD/front-end-dsp/nist/include
export NISTLIB=$PWD/front-end-dsp/nist/lib
cd front-end-dsp/Segmentation ; make -B ; cd ../../
sh src/clean.sh
make -B

