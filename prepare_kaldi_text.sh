working_dir=$PWD
transcription=transcription
awk '{printf("test_100_%04d %s\n", NR, $0)}' $working_dir/${transcription} > $working_dir/transcription.txt
if [ ! -d "$working_dir/audio/*" ]; then rm -rf $working_dir/audio/*; fi
if [ ! -d "$working_dir/Kaldi_template/data/train" ]; then rm -rf $working_dir/Kaldi_template/data/train; fi
if [ ! -d "$working_dir/Kaldi_template/data/test" ]; then rm -rf $working_dir/Kaldi_template/data/test; fi


cp $working_dir/transcription.txt $working_dir/transcription_with_commas.txt

sed -i 's/\.//g' $working_dir/transcription.txt
sed -i 's/,//g' $working_dir/transcription.txt
sed -i 's/;//g' $working_dir/transcription.txt
sed -i 's/://g' $working_dir/transcription.txt
sed -i 's/!//g' $working_dir/transcription.txt
sed -i 's/?//g' $working_dir/transcription.txt
sed -i 's/-/ /g' $working_dir/transcription.txt


cp -r $working_dir/transcription.txt $working_dir/Kaldi_template/phonifier/
bash Kaldi_template/phonifier/phonify_text.sh transcription.txt lexicon.txt 25 $working_dir/Kaldi_template/phonifier
cp $working_dir/Kaldi_template/phonifier/new_text.txt $working_dir/Kaldi_template/data/Tamil/train/transcription.txt
cp $working_dir/Kaldi_template/phonifier/new_text.txt $working_dir/Kaldi_template/data/Tamil/test/transcription.txt
cat $working_dir/Kaldi_template/phonifier/lexicon.txt > $working_dir/Kaldi_template/data/local/lexicon.txt
cat $working_dir/Kaldi_template/phonifier/lexicon.txt > $working_dir/Kaldi_template/data/local/dict/lexicon.txt
echo '!SIL sil' >> $working_dir/Kaldi_template/data/local/dict/lexicon.txt
sort -u -o $working_dir/Kaldi_template/data/local/dict/lexicon.txt $working_dir/Kaldi_template/data/local/dict/lexicon.txt
