#!/usr/bin/env python
#  Created by Mano Ranjith Kumar on 4/07/22
#  cs19s032@smail.iitm.ac.in
#  Input : Transcription after phrasification
#  Output : Transcription with star similar to festival txt.done.data_new

import sys
filename=sys.argv[1]
with open(filename,"r") as fp:
	lines=fp.read().splitlines()
content=""
header=""
for i in range(len(lines)):
	line = lines[i]
	header = line.strip().split("-")[0]
	if(content==""):
		content += " ".join(line.strip().split()[1:])
	else:
		content = content+" * "+" ".join(line.strip().split()[1:])
	if(i<(len(lines)-1)):
		next_header = lines[i+1].strip().split("-")[0]
		if(next_header!=header):
			with open(filename+"_new","a") as fp:
				fp.write(str(header)+" "+content+"\n")
				content=""
if(content!=""):
	with open(filename+"_new","a") as fp:
		fp.write(str(header)+" "+content+"\n")
		content=""
