#!/usr/bin/python3

# TTS using API synthesis code
# Author: Jom Kuriakose
# email: jom@cse.iitm.ac.in
# Date: 16/02/2021

import requests
import shutil
import wave
import json
import wget
import sys
import os

def main():

	# Read variables
	textFile = sys.argv[1]
	audioFile = sys.argv[2]
	lang = sys.argv[3]
	gender = sys.argv[4]

	tempFolderName = os.path.splitext(audioFile)[0]
	
	try:
		os.makedirs(tempFolderName)
	except OSError as e:
		print(tempFolderName)
		sys.exit('Error: Output already exists!!')
		
	# Read text file
	fid = open(textFile,'r')
	text = fid.read().strip()
	text_split = text.split('\n')
	num_lines = len(text_split)
	fid.close()
	
	# IITM API
	if (num_lines <= 1):
		wav_list = IITM_API_request(text,gender,lang)
	else:
		n = 1
		text_split_final = [text_split[i * n:(i + 1) * n] for i in range((len(text_split) + n - 1) // n)]
		wav_list = []
		print(text_split_final)
		for i in range(0,len(text_split_final)):
			wav_list_split = IITM_API_request('\n'.join(text_split_final[i]),gender,lang)
			wav_list.extend(wav_list_split)

	# Output audio
	fileList = []
	for i in range(0,len(wav_list)):
		url = wav_list[i]
		filename = wget.download(url, out=tempFolderName)
		only_name = os.path.splitext(filename)[0]
		directories = only_name.split("/")
		just_filename = directories[-1]
		substring = just_filename.split("_")
		substring[1] = "100"
		substring[2] = f"{i+1:04}"
		new_name = "_".join(substring)
		new_name +=".wav"
		directories[-1]=new_name
		my_new_name = "/".join(directories)
		#print(filename,new_name)
		os.rename(filename, my_new_name)
		fileList.append(filename)
	
	#output_data= []
	#for infile in fileList:
	#	w = wave.open(infile, 'rb')
	#	output_data.append( [w.getparams(), w.readframes(w.getnframes())] )
	#	w.close()
    
	#output = wave.open(audioFile, 'wb')
	#output.setparams(output_data[0][0])
	#for i in range(len(output_data)):
	#	output.writeframes(output_data[i][1])
	#output.close()

	#shutil.rmtree(tempFolderName)

# API function
def IITM_API_request(text,gender,lang):
	url = "https://asr.iitm.ac.in/fs2"
	payload = json.dumps({
		"text": text,
		"gender": gender,
		"lang": lang,
		"speed":1.2
	})
	headers = {
		'Content-Type': 'application/json'
	}
	response = requests.request("POST", url, headers=headers, data=payload)
	if (response.json()['status'] == 'success'):
		wav_list_str = response.json()['outspeech_filepath']
	else:
		sys.exit(response.json()['reason'])
	wav_list = wav_list_str.replace('"','').strip('][').split(', ')
	return wav_list

if __name__ == "__main__":
	main()

