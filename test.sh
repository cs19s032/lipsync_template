#!/bin/bash
##############################################################################
#										#	
#	created by mano ranjith kumar m					#	
#										#
#	date : April 8 2022							#
#										#
#	email : cs19s032@cse.iitm.ac.in					#
#										#
#	Description: This code is the wrapper for running kaldi and Hybrid	#
#	segmentation. 								#
#										#
#	Arguments:								#
#		(1) Language of the synthesis					#
#		(2) Gender of the required synthesis				#
#										#
#	Input:									#
#		The script expects you to enter the model path		#
#		which contains espnet as well as waveglow	 		#
#										#
#										#		
##############################################################################


set -e
lang=$1
gender=$2
echo "Please enter waveglow and espnet path : (ex: /tts1/tts1/anusha/TTS_models/$1_$2)"
read model_path
bash run_kaldi.sh $1 $2 $model_path
bash run_hybrid.sh
set +e
